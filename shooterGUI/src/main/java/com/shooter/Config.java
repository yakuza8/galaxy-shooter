package main.java.com.shooter;

import java.io.IOException;
import java.util.Properties;

@SuppressWarnings("WeakerAccess")
public class Config {

    // Rest and GameServer Config

    public static String getGameUrl() {
        Properties properties = new Properties();
        try {
            properties.load(Config.class.getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty("webserver.url", "http://localhost:8080/");
    }

    public static String getGameHost() {
        Properties properties = new Properties();
        try {
            properties.load(Config.class.getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty("server.host", "localhost");
    }

    public static int getGamePort() {
        Properties properties = new Properties();
        try {
            properties.load(Config.class.getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(properties.getProperty("server.port", "8082"));
    }

    static public final String GAMESERVER_HOST = getGameHost();
    static public final int GAMESERVER_PORT = getGamePort();

    // Player Roles
    public static final int masterRole = 0;
    @SuppressWarnings("unused")
    public static final int slaveRole = 1;

    // Screen size settings
    public static final int WIDTH = 550;
    public static final int HEIGHT = 700;

    // Timeline settings
    public static final int TICK_INTERVAL = 25;

    // Game related settings
    public static final int MAX_LEVEL = 3;
    public static final int ENEMY_COUNT = 3;

    public static final double ENEMY_SMALL_SCORE = 10;
    public static final double ENEMY_MEDIUM_SCORE = 20;
    public static final double ENEMY_LARGE_SCORE = 30;
    public static final double ENEMY_BOSS_SCORE = 150;

    public static final int PLAYER_HEALTH = 10;
    public static final int ENEMY_SMALL_HEALTH = 1;
    public static final int ENEMY_MEDIUM_HEALTH = 2;
    public static final int ENEMY_LARGE_HEALTH = 3;
    public static final int ENEMY_BOSS_HEALTH = 35;

    public static final int PLAYER_FIRE_RATE = 20;
    public static final int ENEMY_FIRE_RATE = 30;

    public static final double ENEMY_SPAWN_CHANCE = 0.035;
    public static final double ENEMY_SMALL_SPAWN_CHANCE = 0.030;
    public static final double ENEMY_MEDIUM_SPAWN_CHANCE = 0.030;
    public static final double ENEMY_LARGE_SPAWN_CHANCE = 0.030;

    public static final double BULLET_PLAYER_SPAWN_CHANCE = 1;
    public static final double BULLET_ENEMY_SPAWN_CHANCE_DEFAULT = 0.2;
    public static final double BULLET_ENEMY_BOSS_SPAWN_CHANCE_DEFAULT = 1.0;
    public static final double UPGRADE_SPAWN_CHANCE = 0.15;

    public static final double ENEMY_SMALL_SPEED = 3.1;
    public static final double ENEMY_MEDIUM_SPEED = 2.3;
    public static final double ENEMY_LARGE_SPEED = 1.4;
    public static final double ENEMY_BOSS_SPEED = 0.9;

    public static final double BULLET_PLAYER_SPEED = 12;
    public static final double BULLET_ENEMY_SPEED = 10;
    public static final double UPGRADE_SPEED = 5;

    // Level coefficients
    public static final double LEVEL_TIME = 30;
    public static final double LEVEL_TIME_DELTA_BEFORE_INCREASE = 3.0;
    public static final double LEVEL_DIFFICULTY_DELTA_BEFORE_CHANGE = 0.1;
    public static final int    MULTI_PLAYER_LEVEL = 4;

    // Sound volumes
    public static final double BGM_VOLUME = 0.30;
    public static final double BGM_INTRO_VOLUME = 0.60;
    public static final double SOUND_SHOOT_PLAYER_VOLUME = 0.10;
    public static final double SOUND_SHOOT_ENEMY_VOLUME = 0.13;
    public static final double SOUND_EXPLOSION_VOLUME = 0.14;

    // Path Variables
    public static final String IMAGE_BACKGROUND_PATH = "/image/background.png";
    public static final String IMAGE_PLAYER_PATH = "/image/player.png";
    public static final String IMAGE_PLAYER2_PATH = "/image/player2.png";
    public static final String IMAGE_ENEMY_SMALL_PATH = "/image/enemy1.png";
    public static final String IMAGE_ENEMY_MEDIUM_PATH = "/image/enemy2.png";
    public static final String IMAGE_ENEMY_LARGE_PATH = "/image/enemy3.png";
    public static final String IMAGE_ENEMY_BOSS_PATH = "/image/enemy_boss.png";
    public static final String BULLET_PLAYER_PATH = "/image/bullet_player.png";
//    public static final String BULLET_PLAYER2_PATH = "/image/bullet_player2.png";
    public static final String BULLET_ENEMY_PATH = "/image/bullet_enemy.png";
    public static final String UPGRADE_LIFE_PATH = "/image/upgrade_health.png";
    public static final String UPGRADE_FIRERATE_PATH = "/image/upgrade_firerate.png";

    public static final String BGM_PATH = "/sound/bgm.mp3";
    public static final String BGM_INTRO_PATH = "/sound/bgm_intro.mp3";
    public static final String SOUND_SHOOT_PLAYER_PATH = "/sound/shoot.wav";
    public static final String SOUND_SHOOT_ENEMY_PATH = "/sound/shoot_enemy.wav";
    public static final String SOUND_EXPLOSION_PATH = "/sound/explosion.wav";

    public static final String VIEW_AUTH_SCREEN_PATH = "/view/AuthScreen.fxml";
    public static final String VIEW_START_SCREEN_PATH = "/view/StartScreen.fxml";
    public static final String VIEW_LEADERBOARD_SCREEN_PATH = "/view/LeaderboardScreen.fxml";
    public static final String VIEW_ENDGAME_SCREEN_PATH = "/view/EndGameScreen.fxml";

}
