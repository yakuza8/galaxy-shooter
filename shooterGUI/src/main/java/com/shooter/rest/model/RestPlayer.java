package main.java.com.shooter.rest.model;

/**
 * Class to handle RestPlayer entity in Scores with RestAPI
 */
@SuppressWarnings("unused")
public class RestPlayer {
    private Integer id;

    private String nickname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "RestPlayer{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
