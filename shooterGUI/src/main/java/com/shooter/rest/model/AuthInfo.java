package main.java.com.shooter.rest.model;

/**
 * Class to handle Authentication Information Exchange with RestAPI
 */
@SuppressWarnings("unused")
public class AuthInfo {

    private Integer id;
    private String nickname;
    private String password;

    public AuthInfo(String nickname, String password) {
        this.nickname = nickname;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public String getPassword() {
        return password;
    }
}
