package main.java.com.shooter.rest.model;

import java.util.Date;

/**
 * Class to handle Score Exchange with RestAPI
 */
@SuppressWarnings("unused")
public class ScoreAbstract {

    private Integer score_id;

    private Integer player_id;

    private Integer score;

    private Date date;

    private RestPlayer player;

    public Integer getScore_id() {
        return score_id;
    }

    public void setScore_id(Integer score_id) {
        this.score_id = score_id;
    }

    public Integer getPlayer_id() {
        return player_id;
    }

    public void setPlayer_id(Integer player_id) {
        this.player_id = player_id;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public RestPlayer getPlayer() {
        return player;
    }

    public void setPlayer(RestPlayer player) {
        this.player = player;
    }

    @Override
    public String toString() {
        return "ScoreAbstract{" +
                "score_id=" + score_id +
                ", player_id=" + player_id +
                ", score=" + score +
                ", date=" + date +
                ", player=" + player +
                '}';
    }
}
