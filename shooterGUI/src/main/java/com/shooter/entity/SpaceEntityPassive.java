package main.java.com.shooter.entity;

@SuppressWarnings("ALL")
public abstract class SpaceEntityPassive extends SpaceEntity {

    public SpaceEntityPassive(String spriteName, double width, double height) {
        super(spriteName, width, height);
        health = 1;
    }

}
