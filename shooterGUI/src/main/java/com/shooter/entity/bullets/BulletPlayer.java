package main.java.com.shooter.entity.bullets;

import main.java.com.shooter.Config;
import main.java.com.shooter.entity.Bullet;
import main.java.com.shooter.entity.Enemy;
import main.java.com.shooter.entity.Player;
import main.java.com.shooter.entity.SpaceEntity;

public class BulletPlayer extends Bullet {

    private Player player = null;

    public BulletPlayer() {
        super(Config.BULLET_PLAYER_PATH, 9, 30);
    }

    public BulletPlayer(Player player) {
        super(Config.BULLET_PLAYER_PATH, 9, 30);
        this.player = player;
    }

    @Override
    public void move() {
        setY(getY() - Config.BULLET_PLAYER_SPEED);
    }

    @Override
    public void actionCollision(SpaceEntity entity) {
        if (collisionCheck(entity)){
            if (entity instanceof Enemy) {
                takeDamage(1);
                if (!entity.isAlive() && player != null) {
                    player.setScore(player.getScore() + entity.giveScore());
                }
            }
        }
    }

}
