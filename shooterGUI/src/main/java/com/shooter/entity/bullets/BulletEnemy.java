package main.java.com.shooter.entity.bullets;

import main.java.com.shooter.Config;
import main.java.com.shooter.entity.Bullet;
import main.java.com.shooter.entity.Player;
import main.java.com.shooter.entity.SpaceEntity;

public class BulletEnemy extends Bullet {

    public BulletEnemy() {
        super(Config.BULLET_ENEMY_PATH, 9, 9);
    }

    @Override
    public void move() {
        setY(getY() + Config.BULLET_ENEMY_SPEED);
    }

    @Override
    public void actionCollision(SpaceEntity entity) {
        if (collisionCheck(entity)){
            if (entity instanceof Player) {
                takeDamage(1);
            }
        }
    }

}
