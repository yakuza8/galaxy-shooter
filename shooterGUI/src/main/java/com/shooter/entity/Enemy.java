package main.java.com.shooter.entity;

import javafx.scene.media.AudioClip;
import main.java.com.shooter.Config;
import main.java.com.shooter.entity.bullets.BulletEnemy;
import main.java.com.shooter.entity.bullets.BulletPlayer;
import main.java.com.shooter.entity.upgrades.UpgradeFireRate;
import main.java.com.shooter.entity.upgrades.UpgradeHealth;

import java.util.ArrayList;
import java.util.Random;

@SuppressWarnings("ALL")
public abstract class Enemy extends SpaceEntityActive {

    private static double DEFAULT_IMAGE_WIDTH  = 36;
    private static double DEFAULT_IMAGE_HEIGHT = 28;

    protected enum DIRECTION {
        LEFT, RIGHT
    }

    @SuppressWarnings("FieldCanBeLocal")
    private int timerCounterMax = Config.ENEMY_FIRE_RATE, timerCounter = Config.ENEMY_FIRE_RATE;
    private double bulletSpawnRate;
    private AudioClip shootSound;
    private AudioClip explodeSound;

    public Enemy(int health, String spriteName, double bulletSpawnRate) {
        super(spriteName, DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT, health);
        this.bulletSpawnRate = bulletSpawnRate;
        setSound();
    }

    public Enemy(int health, String spriteName, int width, int height, double bulletSpawnRate) {
        super(spriteName, width, height, health);
        this.bulletSpawnRate = bulletSpawnRate;
        setSound();
    }

    private void setSound() {
        shootSound = new AudioClip(this.getClass().getResource(Config.SOUND_SHOOT_ENEMY_PATH).toExternalForm());
        shootSound.setVolume(Config.SOUND_SHOOT_ENEMY_VOLUME);
        explodeSound = new AudioClip(this.getClass().getResource(Config.SOUND_EXPLOSION_PATH).toExternalForm());
        explodeSound.setVolume(Config.SOUND_EXPLOSION_VOLUME);
    }

    @Override
    public void shoot(ArrayList<SpaceEntity> toAdd) {
        if (--timerCounter != 0)
            return;

        timerCounter = timerCounterMax;
        if ((new Random()).nextDouble() < bulletSpawnRate) {
            Bullet bullet = new BulletEnemy();
            bullet.setX(getCenterX());
            bullet.setY(getCenterY() - getHeight() / 2 - 1 );

            toAdd.add(bullet);
            shootSound.play();
        }
    }

    @Override
    public void actionCollision(SpaceEntity entity) {
        if (collisionCheck(entity)){
            if (entity instanceof Player || entity instanceof BulletPlayer) {
                takeDamage(1);
            }
        }
    }

    @Override
    public void actionDead(ArrayList<SpaceEntity> toAdd) {
        explodeSound.play();
        if ((new Random()).nextDouble() < Config.UPGRADE_SPAWN_CHANCE) {
            Upgrade upgrade;
            if ((new Random().nextDouble() < 0.5))
                upgrade = new UpgradeHealth();
            else
                upgrade = new UpgradeFireRate();
            upgrade.setX(getCenterX());
            upgrade.setY(getCenterY() - getHeight() / 2 - 1);
            toAdd.add(upgrade);
        }

    }

}
