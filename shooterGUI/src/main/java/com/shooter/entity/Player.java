package main.java.com.shooter.entity;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.media.AudioClip;
import main.java.com.shooter.Config;
import main.java.com.shooter.entity.bullets.BulletEnemy;
import main.java.com.shooter.entity.bullets.BulletPlayer;
import main.java.com.shooter.entity.upgrades.UpgradeFireRate;
import main.java.com.shooter.entity.upgrades.UpgradeHealth;

import java.util.ArrayList;
import java.util.Random;

@SuppressWarnings("ALL")
public class Player extends SpaceEntityActive {

    private int timerCounterMax = Config.PLAYER_FIRE_RATE, timerCounter = Config.PLAYER_FIRE_RATE;

    private boolean autoShoot = true;
    private AudioClip shootSound;

    private Integer score = 0;
    private double currentX, currentY;

    public Player(boolean meOrOppenent) {
        super(meOrOppenent ? Config.IMAGE_PLAYER_PATH : Config.IMAGE_PLAYER2_PATH, 48, 36 , Config.PLAYER_HEALTH);
        setX((double) Config.WIDTH / 2);
        setY(Config.HEIGHT - height);

        shootSound = new AudioClip(this.getClass().getResource(Config.SOUND_SHOOT_PLAYER_PATH).toExternalForm());
        shootSound.setVolume(Config.SOUND_SHOOT_PLAYER_VOLUME);
    }

    public Player setPane(Pane pane) {
        EventHandler<MouseEvent> moveEventHandler = e -> {
            if (e.getX() < Config.WIDTH - getWidth())
                setX(e.getX());
            if (e.getY() < Config.HEIGHT - getHeight())
                setY(e.getY());
        };
        pane.setOnMouseMoved(moveEventHandler);
        pane.setOnMouseDragged(moveEventHandler);

        pane.setOnMousePressed(e -> {
            if (e.isSecondaryButtonDown()){
                autoShoot = !autoShoot;
                this.setAutoShoot(autoShoot);
            }
        });
        return this;
    }

    public boolean getAutoShoot() {
        return autoShoot;
    }

    public Player setAutoShoot(boolean autoShoot) {
        if (autoShoot) {
            timerCounter = 1;
        }
        else {
            timerCounter = Integer.MAX_VALUE;
        }
        this.autoShoot = autoShoot;
        return this;
    }

    public Integer getScore() {
        return score;
    }

    public Player setScore(Integer score) {
        this.score = score;
        return this;
    }

    public Player setHealth(int health) {
        super.health = Math.max(health, 0);
        return this;
    }

    public double getCurrentX() {
        return currentX;
    }

    public Player setCurrentX(double currentX) {
        this.currentX = currentX;
        return this;
    }

    public double getCurrentY() {
        return currentY;
    }

    public Player setCurrentY(double currentY) {
        this.currentY = currentY;
        return this;
    }

    @Override
    public void move() {
        // Do nothing since it is handled and bound in constructor
    }

    @Override
    public void shoot(ArrayList<SpaceEntity> toAdd) {
        if (--timerCounter > 0)
            return;

        timerCounter = timerCounterMax;
        if ((new Random()).nextDouble() < Config.BULLET_PLAYER_SPAWN_CHANCE) {
            Bullet bullet = new BulletPlayer(this);
            bullet.setX(getCenterX() - bullet.getWidth() / 2);
            bullet.setY(getCenterY() - bullet.getHeight() / 2 - 1 );

            toAdd.add(bullet);

            shootSound.play();
        }
    }

    @Override
    public void actionCollision(SpaceEntity entity) {
        if (collisionCheck(entity)){
            if (entity instanceof Enemy || entity instanceof BulletEnemy) {
                takeDamage(1);
            }
            if (entity instanceof UpgradeHealth && health < Config.PLAYER_HEALTH) {
                takeDamage(-1);
            }
            if (entity instanceof UpgradeFireRate && timerCounterMax > Config.PLAYER_FIRE_RATE / 2) {
                timerCounterMax -= 2;
            }
        }
    }

    public double getHealthRatio() {
        return (double) getHealth() / Config.PLAYER_HEALTH;
    }

}