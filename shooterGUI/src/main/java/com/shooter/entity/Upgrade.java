package main.java.com.shooter.entity;

import main.java.com.shooter.Config;

@SuppressWarnings("ALL")
public abstract class Upgrade extends SpaceEntityPassive {
    public Upgrade(String spriteName, double width, double height) {
        super(spriteName, width, height);
    }

    @Override
    public void move() {
        setY(getY() + Config.UPGRADE_SPEED);
    }

    @Override
    public void actionCollision(SpaceEntity entity) {
        if (collisionCheck(entity)){
            if (entity instanceof Player) {
                takeDamage(1);
            }
        }
    }
}
