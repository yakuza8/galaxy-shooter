package main.java.com.shooter.entity.enemies;

import main.java.com.shooter.Config;
import main.java.com.shooter.entity.Enemy;

import java.util.Random;

@SuppressWarnings("FieldCanBeLocal")
public class EnemyBoss extends Enemy {

    private DIRECTION direction;
    private final double horizontalSpeed = 3.1;
    private static final int imageWidth  = 194;
    private static final int imageHeight = 130;
    private static final int maxHeightToContinue = 35;

    public EnemyBoss() {
        super(Config.ENEMY_BOSS_HEALTH, Config.IMAGE_ENEMY_BOSS_PATH, imageWidth, imageHeight, Config.BULLET_ENEMY_BOSS_SPAWN_CHANCE_DEFAULT);
        direction = new Random().nextBoolean() ? DIRECTION.LEFT : DIRECTION.RIGHT;
    }

    @Override
    public void move() {
        if (getY() < maxHeightToContinue) {
            setY(getY() + Config.ENEMY_BOSS_SPEED);
        } else {
            if (direction == DIRECTION.LEFT) {
                double amount = getX() - horizontalSpeed * Config.ENEMY_BOSS_SPEED;
                if (amount > 0) {
                    setX(amount);
                } else {
                    direction = DIRECTION.RIGHT;
                }
            } else {
                double amount = getX() + horizontalSpeed * Config.ENEMY_BOSS_SPEED;
                if (amount < Config.WIDTH - getWidth()) {
                    setX(amount);
                } else {
                    direction = DIRECTION.LEFT;
                }
            }
        }
    }

    @Override
    public int giveScore() {
        return (int) Config.ENEMY_BOSS_SCORE;
    }

}
