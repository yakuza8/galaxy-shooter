package main.java.com.shooter.entity.enemies;

import main.java.com.shooter.Config;
import main.java.com.shooter.entity.Enemy;

@SuppressWarnings("ALL")
public class EnemyMedium extends Enemy {

    @SuppressWarnings("FieldCanBeLocal")
    private int MAX_DIRECTION_COUNT = 15;
    private DIRECTION direction;
    private int directionCount;
    private int timerCounter = 2;
    private double horizontalSpeed = 1.1;

    public EnemyMedium() {
        super(Config.ENEMY_MEDIUM_HEALTH, Config.IMAGE_ENEMY_MEDIUM_PATH, Config.BULLET_ENEMY_SPAWN_CHANCE_DEFAULT);
        direction = DIRECTION.LEFT;
        directionCount = 0;
    }

    @Override
    public void move() {
        setY(getY() + Config.ENEMY_MEDIUM_SPEED);

        if (--timerCounter != 0)
            return;
        timerCounter = 5;

        if (direction == DIRECTION.LEFT) {
            double amount = getX() - horizontalSpeed * Config.ENEMY_MEDIUM_SPEED;
            setX(amount >= 0 ? amount : 0);
        } else {
            double amount = getX() + horizontalSpeed * Config.ENEMY_MEDIUM_SPEED;
            setX(amount <= Config.WIDTH - getWidth() ? amount : Config.WIDTH - getWidth());
        }
        directionCount++;
        if (directionCount == MAX_DIRECTION_COUNT) {
            directionCount = 0;
            direction = direction == DIRECTION.LEFT ? DIRECTION.RIGHT : DIRECTION.LEFT;
        }
    }

    @Override
    public int giveScore() {
        return (int) Config.ENEMY_MEDIUM_SCORE;
    }
}
