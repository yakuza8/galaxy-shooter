package main.java.com.shooter.entity.enemies;

import main.java.com.shooter.Config;
import main.java.com.shooter.entity.Enemy;

public class EnemyLarge extends Enemy {

    public EnemyLarge() {
        super(Config.ENEMY_LARGE_HEALTH, Config.IMAGE_ENEMY_LARGE_PATH, Config.BULLET_ENEMY_SPAWN_CHANCE_DEFAULT);
    }

    @Override
    public void move() {
        setY(getY() + Config.ENEMY_LARGE_SPEED);
    }

    @Override
    public int giveScore() {
        return (int) Config.ENEMY_LARGE_SCORE;
    }

}
