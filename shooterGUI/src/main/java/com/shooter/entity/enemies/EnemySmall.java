package main.java.com.shooter.entity.enemies;

import main.java.com.shooter.Config;
import main.java.com.shooter.entity.Enemy;

public class EnemySmall extends Enemy {

    public EnemySmall() {
        super(Config.ENEMY_SMALL_HEALTH, Config.IMAGE_ENEMY_SMALL_PATH, Config.BULLET_ENEMY_SPAWN_CHANCE_DEFAULT);
    }

    @Override
    public void move() {
        setY(getY() + Config.ENEMY_SMALL_SPEED);
    }

    @Override
    public int giveScore() {
        return (int)Config.ENEMY_SMALL_SCORE;
    }

}
