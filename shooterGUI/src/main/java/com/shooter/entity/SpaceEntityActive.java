package main.java.com.shooter.entity;

@SuppressWarnings("ALL")
public abstract class SpaceEntityActive extends SpaceEntity {


    public SpaceEntityActive(String spriteName, double width, double height, int health) {
        super(spriteName, width, height);
        this.health = health;
    }

}
