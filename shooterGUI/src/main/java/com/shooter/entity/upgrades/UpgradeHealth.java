package main.java.com.shooter.entity.upgrades;

import main.java.com.shooter.Config;
import main.java.com.shooter.entity.Upgrade;

public class UpgradeHealth extends Upgrade {

    public UpgradeHealth() {
        super(Config.UPGRADE_LIFE_PATH, 16, 16);
    }

}
