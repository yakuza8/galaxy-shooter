package main.java.com.shooter.entity.upgrades;

import main.java.com.shooter.Config;
import main.java.com.shooter.entity.Upgrade;

public class UpgradeFireRate extends Upgrade {

    public UpgradeFireRate() {
        super(Config.UPGRADE_FIRERATE_PATH, 16, 16);
    }

}
