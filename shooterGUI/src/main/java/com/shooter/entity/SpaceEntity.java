package main.java.com.shooter.entity;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import main.java.com.shooter.Config;

import java.util.ArrayList;

@SuppressWarnings("ALL")
public abstract class SpaceEntity extends ImageView {

    protected double width = 32;
    protected double height = 24;
    protected Image sprite;
    protected int health;

    public SpaceEntity(String spriteName, double width, double height) {
        this.width = width;
        this.height = height;
        sprite = new Image(spriteName, this.width, this.height, false, false);
        super.setImage(sprite);
        setFitHeight(this.height);
        setFitWidth(this.width);
    }

    public abstract void move();

    public void shoot(ArrayList<SpaceEntity> toAdd) {}

    public void actionDead(ArrayList<SpaceEntity> toAdd) {}

    public boolean collisionCheck(SpaceEntity entity) {
        return intersects(entity.getX(), entity.getY(), entity.getWidth(), entity.getHeight());
    }

    public abstract void actionCollision(SpaceEntity entity);

    public boolean isAlive() {
        return getHealth() > 0;
    }

    public boolean isInScreen() {
        return getX() > 0 - getWidth() &&
                getX() < Config.WIDTH + getWidth() &&
                getY() > 0 - getHeight() &&
                getY() < Config.HEIGHT + getHeight();
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getCenterX() {
        return getX() + (width / 2);
    }

    public double getCenterY() {
        return getY() + (height / 2);
    }

    public int getHealth() {
        return health;
    }

    public void takeDamage(int damage) {
        health = Math.max(health - damage, 0);
    }

    public int giveScore() {
        return 0;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " " + this.getX() + " " + this.getY();
    }
}

