package main.java.com.shooter.entity;

@SuppressWarnings("ALL")
public abstract class Bullet extends SpaceEntityPassive {

    public Bullet(String spriteName, double width, double height) {
        super(spriteName, width, height);
    }

}
