package main.java.com.shooter;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;

@SuppressWarnings("ALL")
/**
 * Message protocol class
 * It has necessary message classes like StartGame, EndGame, PlayerInformation and EntityPopulation
 * It must first register to those classes in order to serialize them
 * Then you can use them with Kryonet API
 *
 * Start game part forwards roles to players in order to decide master/slave roles
 * Player information part sends players' health, score, position and autoshoot information
 * Entity population part supports parsing and assembling tranmissted entities
 * End game part forwards scores to player with each other's scores
 */
public class MessageProtocol {

    // This registers objects that are going to be sent over the network.
    static public void register (EndPoint endPoint) {
        Kryo kryo = endPoint.getKryo();
        kryo.register(StartGame.class);
        kryo.register(EndGame.class);
        kryo.register(PlayersInfo.class);
        kryo.register(Entities.class);
        kryo.register(String[].class);
    }

    static public class Entities {
        public String[] allEntities;

        public Entities setAllEntities(String[] allEntities) {
            this.allEntities = allEntities;
            return this;
        }
    }

    static public class PlayersInfo {
        public double x, y;
        public int health, score;
        public boolean autoshoot;
        public double x2, y2;
        public int health2, score2;
        public boolean autoshoot2;

        public PlayersInfo() { }

        public PlayersInfo(double x, double y, int health, int score, boolean autoshoot,
                           double x2, double y2, int health2, int score2, boolean autoshoot2) {
            this.x = x;
            this.y = y;
            this.health = health;
            this.score = score;
            this.autoshoot = autoshoot;
            this.x2 = x2;
            this.y2 = y2;
            this.health2 = health2;
            this.score2 = score2;
            this.autoshoot2 = autoshoot2;
        }
    }

    static public class StartGame {
        public int role;

        public StartGame setRole(int role) {
            this.role = role;
            return this;
        }
    }

    static public class EndGame {
        public int score1;
        public int score2;

        public EndGame setScore1(int score1) {
            this.score1 = score1;
            return this;
        }

        public EndGame setScore2(int score2) {
            this.score2 = score2;
            return this;
        }
    }

}