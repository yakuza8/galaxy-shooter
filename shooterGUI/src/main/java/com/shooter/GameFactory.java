package main.java.com.shooter;

import javafx.scene.layout.Pane;
import main.java.com.shooter.entity.*;
import main.java.com.shooter.entity.enemies.EnemyBoss;
import main.java.com.shooter.entity.enemies.EnemyLarge;
import main.java.com.shooter.entity.enemies.EnemyMedium;
import main.java.com.shooter.entity.enemies.EnemySmall;

import java.util.Random;

@SuppressWarnings("SameParameterValue")
public class GameFactory {

    private static final GameFactory instance = new GameFactory();

    private static final Random random = new Random();

    private Level level = new Level();

    private GameFactory() {}

    public static GameFactory getInstance() {
        return instance;
    }

    private Integer getRandomInt(int bound1, int bound2) {
        return random.nextInt(bound2 - bound1) + bound1;
    }

    private int randomWithRange(int min, int max) {
        int range = (max - min) + 1;
        return (int)(Math.random() * range) + min;
    }

    /**
     * RestPlayer spawn function
     * @param pane: To be able to get binding with mouse movements
     * @return RestPlayer
     */
    public Player spawnPlayer(Pane pane) {
        return new Player(true).setPane(pane);
    }

    /**
     * Randomized enemy factory method
     * @return Created enemy
     */
    private Enemy randomEnemyCreator() {
        int enemyType = randomWithRange(1, Config.ENEMY_COUNT);
        Enemy enemy = null;
        switch (enemyType) {
            case 1:
                enemy = new EnemySmall();
                break;
            case 2:
                enemy = new EnemyMedium();
                break;
            case 3:
                enemy = new EnemyLarge();
                break;
            default:
                System.out.println("Unknown enemy type!");
                break;
        }
        return enemy;
    }

    /**
     * Enemy spawn function
     * @return Enemy
     */
    public Enemy spawnEnemy() {
        if (Math.random() < Config.ENEMY_SPAWN_CHANCE) {
            Enemy enemy = randomEnemyCreator();
            enemy.setX(getRandomInt(0, (int) (Config.WIDTH - enemy.getWidth())));
            enemy.setY((int) -enemy.getHeight());
            return enemy;
        }
        return null;
    }

    /**
     * Enemy boss spawn function
     * @return EnemyBoss
     */
    public Enemy spawnEnemyBoss() {
        Enemy enemyBoss =  new EnemyBoss();
        enemyBoss.setX(getRandomInt(0, (int) (Config.WIDTH - enemyBoss.getWidth())));
        enemyBoss.setY((int) - enemyBoss.getHeight());
        level.setEnemyBoss(enemyBoss);
        return enemyBoss;
    }

    /**
     * Build level with respect to given parameter
     * Set difficulty and time limits by given level
     * @param levelNo: Level number of new level
     */
    public void buildLevel(int levelNo) {
        int levelBase = levelNo - 1;
        level = new Level()
                .setLevelNo(levelNo)
                .setGameOngoing(true)
                .setBossSpawned(levelNo != Config.MULTI_PLAYER_LEVEL)
                .setLevelTime(Config.LEVEL_TIME + levelBase * Config.LEVEL_TIME / Config.LEVEL_TIME_DELTA_BEFORE_INCREASE)
                .setBulletEnemySpawnChance(Config.BULLET_ENEMY_SPAWN_CHANCE_DEFAULT + levelBase * Config.LEVEL_DIFFICULTY_DELTA_BEFORE_CHANGE * Config.BULLET_ENEMY_SPAWN_CHANCE_DEFAULT)
                .setEnemySpawnChance(Config.ENEMY_SPAWN_CHANCE + levelBase * Config.LEVEL_DIFFICULTY_DELTA_BEFORE_CHANGE * Config.ENEMY_SPAWN_CHANCE)
                .setEnemySmallScore(Config.ENEMY_SMALL_SCORE + levelBase * Config.LEVEL_DIFFICULTY_DELTA_BEFORE_CHANGE * Config.ENEMY_SMALL_SCORE)
                .setEnemyMediumScore((Config.ENEMY_MEDIUM_SCORE + levelBase * Config.LEVEL_DIFFICULTY_DELTA_BEFORE_CHANGE * Config.ENEMY_MEDIUM_SCORE))
                .setEnemyLargeScore(Config.ENEMY_LARGE_SCORE + levelBase * Config.LEVEL_DIFFICULTY_DELTA_BEFORE_CHANGE * Config.ENEMY_LARGE_SCORE);
    }

    /**
     * Get current level from GameFactory
     * @return current level information
     */
    public Level getLevel() {
        return level;
    }
}
