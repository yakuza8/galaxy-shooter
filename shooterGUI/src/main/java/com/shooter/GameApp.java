package main.java.com.shooter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import main.java.com.shooter.controller.GameController;

public class GameApp extends Application {


    private static Stage pStage;

    private static Scene pScene;

    @Override
    public void start(Stage primaryStage) throws Exception{
        setPrimaryStage(primaryStage);

        // Set title
        primaryStage.setTitle("Galaxy Shooter");

        // Set initial screen
        Parent root = FXMLLoader.load(getClass().getResource(Config.VIEW_AUTH_SCREEN_PATH));
        Scene primaryScene = new Scene(root, Config.WIDTH, Config.HEIGHT);
        primaryScene.getStylesheets().add("stylesheet/alien_shooter_styles.css");
        setPrimaryScene(primaryScene);
        primaryStage.setScene(pScene);
        primaryStage.setResizable(false);
        GameController.playBgmIntro();
        primaryStage.getIcons().add(new Image("image/player.png"));
        // Then show begins...
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static void setRootToScene(Parent root) {
        pScene.setRoot(root);
    }

    @SuppressWarnings("unused")
    public static Stage getPrimaryStage() {
        return pStage;
    }

    private void setPrimaryStage(Stage pStage) {
        GameApp.pStage = pStage;
    }

    public static Scene getPrimaryScene() {
        return pScene;
    }

    private void setPrimaryScene(Scene pScene) {
        GameApp.pScene = pScene;
    }
}
