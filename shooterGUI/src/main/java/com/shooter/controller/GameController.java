package main.java.com.shooter.controller;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;
import main.java.com.shooter.*;
import main.java.com.shooter.entity.Enemy;
import main.java.com.shooter.entity.SpaceEntity;
import main.java.com.shooter.entity.Player;
import main.java.com.shooter.entity.bullets.BulletEnemy;
import main.java.com.shooter.entity.bullets.BulletPlayer;
import main.java.com.shooter.entity.enemies.EnemyBoss;
import main.java.com.shooter.entity.enemies.EnemyLarge;
import main.java.com.shooter.entity.enemies.EnemyMedium;
import main.java.com.shooter.entity.enemies.EnemySmall;
import main.java.com.shooter.entity.upgrades.UpgradeFireRate;
import main.java.com.shooter.entity.upgrades.UpgradeHealth;
import main.java.com.shooter.rest.model.RestPlayer;
import main.java.com.shooter.rest.model.ScoreAbstract;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

@SuppressWarnings({"unused", "WeakerAccess"})
public class GameController{

    private static final String SCORES_PREFIX = "scores";

    private static Integer playerID;
    @SuppressWarnings("CanBeFinal")
    private boolean isMultiPlayerLevel;

    // Media related fields
    private static final MediaPlayer bgm = new MediaPlayer(new Media(GameController.class.getResource(Config.BGM_PATH).toExternalForm()));
    private static final MediaPlayer bgmIntro = new MediaPlayer(new Media(GameController.class.getResource(Config.BGM_INTRO_PATH).toExternalForm()));

    private Pane pane;
    private Timeline timeline;
    private final GameFactory gameFactory = GameFactory.getInstance();

    // Player related fields
    private Player player;
    private IntegerProperty score;
    private DoubleProperty time;
    private DoubleProperty health;

    // Entity container
    private ArrayList<SpaceEntity> spaceEntities;

    // Multi Player related
    private Client client;
    private boolean waitingOpponent = true;
    private int role;
    private ArrayList<SpaceEntity> spaceEntitiesMirror;
    private Player player2;
    private IntegerProperty score2;
    private DoubleProperty health2;

    /**
     * Construct game with respect to given level
     * @param level: Level to be played
     */
    GameController(int level) {
        isMultiPlayerLevel = level == Config.MULTI_PLAYER_LEVEL;
        if (isMultiPlayerLevel) {
            startLevelMultiPlayer(level);
        }
        else {
            startLevel(level);
        }
    }

    /**
     * Level builder, it takes level information and creates/updates the scene with respect to it
     * @param level: Level to be played
     */
    private void startLevel(int level) {
        stopBgmIntro();
        playBgm();
        // Reset variables
        resetVariables();
        // Level related information updates to use them in entities
        gameFactory.buildLevel(level);
        time.set((int) gameFactory.getLevel().getLevelTime());
        // Update screen and create player
        prepareScene();
        createPlayer(pane);
        if (level == Config.MULTI_PLAYER_LEVEL) {
            // If level is multi player level, then add opponent
            player2 = new Player(false);
            addEntity(player2);
            // And remove anything from current entity container
            if (role != Config.masterRole)
                spaceEntities.clear();
        }
        // Game starts!
        startTimeline();
        // Update scene
        GameApp.setRootToScene(pane);
        GameApp.getPrimaryScene().setCursor(Cursor.NONE);
    }

    /**
     * Level builder, it takes level information and creates/updates the scene with respect to it
     * @param level: Level to be played
     */
    private void startLevelMultiPlayer(int level) {
        // Prepare the screen for initial view
        prepareMultiPlayerScreen();
        // Start communication to the game server
        client = new Client();
        MessageProtocol.register(client);
        client.addListener(new Listener() {
            @Override
            public void received (Connection connection, Object object) {
                if (object instanceof MessageProtocol.StartGame) {
                    // Set roles and game begins...
                    MessageProtocol.StartGame response = (MessageProtocol.StartGame)object;
                    role = response.role;
                    startLevel(Config.MULTI_PLAYER_LEVEL);
                    waitingOpponent = false;
                }
                else if (object instanceof MessageProtocol.Entities) {
                    // Populate slave player (Player2) with entities coming from master player (Player1)
                    MessageProtocol.Entities response = (MessageProtocol.Entities) object;

                    ArrayList<SpaceEntity> spaceEntitiesMirrorTemp = new ArrayList<>();
                    for (String entity : response.allEntities) {
                        SpaceEntity spaceEntity = null;
                        String[] split = entity.split(" ");
                        switch (split[0]) {
                            case "EnemySmall":
                                spaceEntity = new EnemySmall();
                                break;
                            case "EnemyMedium":
                                spaceEntity = new EnemyMedium();
                                break;
                            case "EnemyLarge":
                                spaceEntity = new EnemyLarge();
                                break;
                            case "EnemyBoss":
                                spaceEntity = new EnemyBoss();
                                break;
                            case "BulletEnemy":
                                spaceEntity = new BulletEnemy();
                                break;
                            case "BulletPlayer":
                                spaceEntity = new BulletPlayer();
                                break;
                            case "UpgradeFireRate":
                                spaceEntity = new UpgradeFireRate();
                                break;
                            case "UpgradeHealth":
                                spaceEntity = new UpgradeHealth();
                                break;
                        }
                        if (spaceEntity != null) {
                            spaceEntity.setX(Double.parseDouble(split[1]));
                            spaceEntity.setY(Double.parseDouble(split[2]));
                            spaceEntitiesMirrorTemp.add(spaceEntity);
                        }
                    }
                    spaceEntitiesMirror = spaceEntitiesMirrorTemp;
                }
                else if (object instanceof MessageProtocol.EndGame) {
                    // End game
                    MessageProtocol.EndGame endGame = (MessageProtocol.EndGame) object;
                    gameFactory.getLevel().setGameOngoing(false);
                }
                else if (object instanceof MessageProtocol.PlayersInfo) {
                    // Separate message protocol between players in order to keep their information clearly
                    MessageProtocol.PlayersInfo playerInfo = (MessageProtocol.PlayersInfo) object;
                    player2.setCurrentX(playerInfo.x);
                    player2.setCurrentY(playerInfo.y);
                    if (role != Config.masterRole) {
                        player.setHealth(playerInfo.health2);
                        player2.setHealth(playerInfo.health);
                        player.setScore(playerInfo.score2);
                        player2.setScore(playerInfo.score);
                    }
                }
            }

            @Override
            public void disconnected(Connection connection) {
                // Case of disconnection during the game session
                super.disconnected(connection);
                gameFactory.getLevel().setGameOngoing(false);
            }
        });
        client.start();
        try {
            client.connect(5000, Config.GAMESERVER_HOST, Config.GAMESERVER_PORT, Config.GAMESERVER_PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Timeline while playing game, operates respectively as the following
     * Traverse all entities and check collision
     * Add enemies with its probability from GameFactory
     * Move each entity and shoot if it can, then check it life value, in case of that
     * it becomes dead, then push it to dtoRemove
     * Remove necessary entities if they are dead or exit from the screen
     * Check player health and update health bar
     * Check time to end game or not
     */
    private void startTimeline() {
        timeline = new Timeline(
                new KeyFrame(Duration.millis(Config.TICK_INTERVAL), event -> {

                    if (!isMultiPlayerLevel || role == Config.masterRole) {
                        // Adding and deleting queues
                        ArrayList<SpaceEntity> toAdd = new ArrayList<>();
                        ArrayList<SpaceEntity> toRemove = new ArrayList<>();
                        // Check collisions
                        for (SpaceEntity spaceEntity1 : spaceEntities) {
                            for (SpaceEntity spaceEntity2 : spaceEntities) {
                                spaceEntity1.actionCollision(spaceEntity2);
                            }
                        }
                        // Add enemy
                        Enemy enemy;
                        if ((enemy = gameFactory.spawnEnemy()) != null) {
                            if (time.get() > 0) {
                                toAdd.add(enemy);
                            }
                        }
                        // Perform tick action for each entity
                        for (SpaceEntity spaceEntity : spaceEntities) {
                            // Move entities
                            spaceEntity.move();
                            // Shoot bullets
                            spaceEntity.shoot(toAdd);
                            // Check if entity is alive or in screen
                            if (! (spaceEntity.isAlive() && spaceEntity.isInScreen())) {
                                // It an entity is dead, update score and take its action
                                if (! spaceEntity.isAlive()) {
                                    // score.set(score.get() + spaceEntity.giveScore());
                                    spaceEntity.actionDead(toAdd);
                                }
                                toRemove.add(spaceEntity);
                            }
                        }

                        // Remove from toRemove and add from toAdd entities
                        removeEntities(toRemove);
                        addEntities(toAdd);
                    }

                    // Advance the time
                    if (isMultiPlayerLevel) {
                        double newTime = time.get() - Config.TICK_INTERVAL / 1000.0;
                        if (newTime > 0) {
                            time.set(newTime);
                        } else {
                            time.set(0);
                            if (!gameFactory.getLevel().isBossSpawned()) {
                                gameFactory.getLevel().setBossSpawned(true);
                                Enemy enemyBoss = gameFactory.spawnEnemyBoss();
                                addEntity(enemyBoss);
                            }
                        }
                    } else {
                        time.set(time.get() - Config.TICK_INTERVAL / 1000.0);
                    }

                    // Update player properties
                    score.set(player.getScore());
                    health.set(player.getHealthRatio());
                    if (isMultiPlayerLevel) {
                        // If multi player, then update player2's too
                        score2.set(player2.getScore());
                        health2.set(player2.getHealthRatio());

                        player2.setX(player2.getCurrentX());
                        player2.setY(player2.getCurrentY());

                        // Send necessary information related to players
                        MessageProtocol.PlayersInfo playerInfo = new MessageProtocol.PlayersInfo(
                                player.getX(), player.getY(), player.getHealth(), player.getScore(), player.getAutoShoot(),
                                player2.getX(), player2.getY(), player2.getHealth(), player2.getScore(), player2.getAutoShoot());

                        client.sendUDP(playerInfo);

                        // Master client
                        if (role == Config.masterRole) {
                            client.sendUDP(new MessageProtocol.Entities().setAllEntities(
                                    Arrays.stream(spaceEntities.toArray()).map(Object::toString).toArray(String[]::new)
                            ));
                            if (((player.getHealth() <= 0 && player2.getHealth() <= 0))
                                    || (gameFactory.getLevel().isBossSpawned() && !gameFactory.getLevel().getEnemyBoss().isAlive())){
                                client.sendUDP(new MessageProtocol.EndGame());
                                gameFactory.getLevel().setGameOngoing(false);
                            }
                        }
                        // Slave client
                        else { // Check players' health status and remove/add old/new entities from/to pane
                            if (player.getHealth() <= 0) {
                                pane.getChildren().remove(player);
                            }
                            if (player2.getHealth() <= 0) {
                                pane.getChildren().remove(player2);
                            }
                            try {
                                pane.getChildren().removeAll(spaceEntities);
                                spaceEntities.clear();
                                addEntities(spaceEntitiesMirror);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            player.shoot(new ArrayList<>());
                        }

                        if (!gameFactory.getLevel().isGameOngoing()) {
                            // If game finishes, let the player who has more score points win
                            endGame(player.getScore() < player2.getScore());
                        }
                    } else {
                        // Check if player has no health
                        if (health.get() <= 0) {
                            endGame(true);
                        }
                        if (time.get() <= 0) {
                            endGame(false);
                        }
                    }
                })
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    /**
     * End game handler
     */
    private void endGame(boolean isDeadPlayer) {
        // Reset variables to initial values
        timeline.stop();
        spaceEntities.clear();
        pane.getChildren().clear();
        prepareEndGameScreen(pane, isDeadPlayer, score.getValue());
        stopConnection(isMultiPlayerLevel);

        // Save score to database
        Thread thread = new Thread(() -> saveScore(score.getValue()));
        thread.start();
    }

    /**
     * Variable initializer for resetting game
     */
    private void resetVariables() {
        spaceEntities = new ArrayList<>();
        spaceEntitiesMirror = new ArrayList<>();
        score = new SimpleIntegerProperty(0);
        health = new SimpleDoubleProperty(0);
        score2 = new SimpleIntegerProperty(0);
        health2 = new SimpleDoubleProperty(0);
        time = new SimpleDoubleProperty(0);
    }

    /**
     * Add single entities from entity container
     * @param spaceEntity: Entity to be add
     */
    private void addEntity(SpaceEntity spaceEntity) {
        spaceEntities.add(spaceEntity);
        pane.getChildren().add(spaceEntity);
    }

    /**
     * Add multiple entities to entity container
     * @param toAdd: Entity list to be added
     */
    private void addEntities(ArrayList<SpaceEntity> toAdd) {
        spaceEntities.addAll(toAdd);
        pane.getChildren().addAll(toAdd);
    }

    /**
     * Delete multiple entities from entity container
     * @param toRemove: Entity list to be deleted
     */
    private void removeEntities(ArrayList<SpaceEntity> toRemove) {
        spaceEntities.removeAll(toRemove);
        pane.getChildren().removeAll(toRemove);
    }

    /**
     * Player creator
     * @param pane: Pane to which player will be added
     */
    private void createPlayer(Pane pane) {
        player = gameFactory.spawnPlayer(pane);
        health.set(player.getHealthRatio());
        addEntity(player);
    }

    private void saveScore(int score) {
        try {
            ScoreAbstract scoreAbstract = new ScoreAbstract();
            RestPlayer restPlayer = new RestPlayer();
            restPlayer.setId(playerID);
            scoreAbstract.setPlayer_id(playerID);
            scoreAbstract.setScore(score);
            scoreAbstract.setPlayer(restPlayer);
            String uri = Config.getGameUrl() + SCORES_PREFIX;
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<ScoreAbstract> request = new HttpEntity<>(scoreAbstract);
            @SuppressWarnings("unused") ScoreAbstract returnedScore = restTemplate.postForObject(uri, request, ScoreAbstract.class);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        }
    }

    /**
     * Prepare multi player screen while waiting response of other player
     */
    private void prepareMultiPlayerScreen() {
        // Create pane
        pane = new Pane();
        // Create elements in pane
        prepareBackground(pane);
        waitForOtherPlayer(pane);
        GameApp.setRootToScene(pane);
    }

    /**
     * Screen prepare functionality such that create everything related to pane
     */
    private void prepareScene() {
        // Create pane
        pane = new Pane();
        // Create elements in pane
        prepareBackground(pane);
        prepareHealthBar(pane, isMultiPlayerLevel, true);
        prepareScore(pane, isMultiPlayerLevel, true);
        prepareTime(pane);
        if (isMultiPlayerLevel) {
            prepareHealthBar(pane, true, false);
            prepareScore(pane, true, false);
        }
    }

    /**
     * Prepare background
     * @param pane: Pane to add
     */
    private void prepareBackground(Pane pane) {
        Image image = new Image(Config.IMAGE_BACKGROUND_PATH);
        BackgroundSize backgroundSize = new BackgroundSize(Config.WIDTH, Config.HEIGHT, true, true, true, false);
        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
        Background background = new Background(backgroundImage);
        pane.setBackground(background);
    }

    /**
     * Prepare health bar
     * @param pane: Pane to add
     */
    private void prepareHealthBar(Pane pane, boolean forMultiPlayer, boolean isMine) {
        Label labelForHealth = new Label(forMultiPlayer ? "HEALTH" + (isMine ? 1 : 2) +  ": " : "HEALTH: ");
        setLabelProperty(labelForHealth, isMine ? 15 : Config.WIDTH - 215, pane);
        labelForHealth.setLayoutY(Config.HEIGHT - 44);
        labelForHealth.setTextFill(forMultiPlayer ? (isMine ? Color.rgb(54, 187, 245) : Color.rgb(245, 54, 54)) : Color.WHITE);
        ProgressBar pb = new ProgressBar();
        pb.setPrefHeight(20);
        pb.setLayoutX(isMine ? 115 : Config.WIDTH - 115);
        pb.setLayoutY(Config.HEIGHT - 30);
        pb.progressProperty().bind(isMine ? health : health2);
        pane.getChildren().add(pb);
    }

    /**
     * Prepare score
     * @param pane: Pane to add
     */
    private void prepareScore(Pane pane, boolean forMultiPlayer, boolean isMine) {
        Label labelForScore = new Label(forMultiPlayer ? "SCORE" + (isMine ? 1 : 2) + ": " : "SCORE: ");
        setLabelProperty(labelForScore, isMine ? 15 : Config.WIDTH - 145, pane);
        labelForScore.setTextFill(forMultiPlayer ? (isMine ? Color.rgb(54, 187, 245) : Color.rgb(245, 54, 54)) : Color.WHITE);
        Label label = new Label();
        label.textProperty().bind(!forMultiPlayer || isMine ? score.asString() : score2.asString());
        setLabelProperty(label, !forMultiPlayer || isMine ? 100 : Config.WIDTH - 60, pane);
    }


    /**
     * Prepare progress indicator for multi player part and wait opponent
     * @param pane: Pane to add
     */
    private void waitForOtherPlayer(Pane pane) {
        Label waitingText = new Label("Waiting for other player");
        waitingText.setLayoutX((Config.WIDTH - 205)  / 2.);
        waitingText.setLayoutY(Config.HEIGHT / 2. - 90);
        waitingText.setTextFill(Color.WHITE);
        waitingText.setFont(new Font("Lato", 20));
        pane.getChildren().add(waitingText);
        // ProgressBar pi = new ProgressBar();
        // ProgressIndicator pi = new ProgressIndicator();
        // pi.setLayoutX((Config.WIDTH  - 45) / 2.);
        // pi.setLayoutY((Config.HEIGHT - 120) / 2.);
        // pane.getChildren().add(pi);
        Button cancelButton = new Button("CANCEL");
        cancelButton.setLayoutX((Config.WIDTH  - 75) / 2.);
        cancelButton.setLayoutY(Config.HEIGHT / 2.);
        cancelButton.setId("dark-blue");
        cancelButton.setOnAction(e -> returnStartScreen());
        pane.getChildren().add(cancelButton);
    }

    /**
     * Set label properties
     * @param pane: Pane to add
     */
    private void setLabelProperty(Label label, double rightPadding, Pane pane) {
        label.setTextFill(Color.WHITE);
        label.setAlignment(Pos.TOP_LEFT);
        label.setFont(new Font("Lato", 20));
        label.setPadding(new Insets(10, 0, 0, rightPadding));
        pane.getChildren().add(label);
    }

    /**
     * Prepare time with position values since the screen is static we can give constant numbers
     * @param pane: Pane to add
     */
    private void prepareTime(Pane pane) {
        Label label = new Label();
        label.setTextFill(Color.WHITE);
        label.setAlignment(Pos.TOP_LEFT);
        label.setLayoutX((Config.WIDTH - 20) / 2.);
        label.setLayoutY(0);
        label.setFont(new Font("Lato", 20));
        label.textProperty().bind(time.asString("%.0f"));
        label.setPadding(new Insets(10, 50, 0, 15));
        pane.getChildren().add(label);
    }

    /**
     * Load end game screen
     * @param pane: Pane to refresh
     * @param isDeadPlayer: Value of whether player is dead or not
     * @param score: Value of score in the level
     */
    private void prepareEndGameScreen(Pane pane, boolean isDeadPlayer, int score) {
        gameFactory.getLevel().setPlayerDead(isDeadPlayer);
        gameFactory.getLevel().setLevelScore(score);
        FXMLLoader loader = new FXMLLoader(getClass().getResource(Config.VIEW_ENDGAME_SCREEN_PATH));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        pane.getChildren().add(loader.getRoot());
        GameApp.getPrimaryScene().setCursor(Cursor.DEFAULT);
    }

    /**
     * Return to start screen
     */
    private void returnStartScreen() {
        try {
            stopConnection(isMultiPlayerLevel);
            GameApp.setRootToScene(FXMLLoader.load(getClass().getResource(Config.VIEW_START_SCREEN_PATH)));
            GameController.stopBgm();
            GameController.playBgmIntro();
            GameApp.getPrimaryScene().setCursor(Cursor.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes the connection
     * @param isMultiPlayerLevel: Value whether the level is multi or single player level
     */
    private void stopConnection(boolean isMultiPlayerLevel) {
        if (isMultiPlayerLevel && client != null) {
            client.stop();
        }
    }

    /**
     * Set player id of the session
     * @param pid: Player id
     */
    public static void setPlayerID(Integer pid) {
        playerID = pid;
    }

    /**
     * @return the current root pane
     */
    public Pane getPane() {
        return this.pane;
    }

    // Music related parts
    public static void playBgmIntro() {
        bgmIntro.setCycleCount(MediaPlayer.INDEFINITE);
        bgmIntro.setVolume(Config.BGM_INTRO_VOLUME);
        bgmIntro.play();
    }

    public static void stopBgmIntro() {
        bgmIntro.stop();
    }

    public static void playBgm() {
        bgm.setCycleCount(MediaPlayer.INDEFINITE);
        bgm.setVolume(Config.BGM_VOLUME);
        bgm.play();
    }

    public static void stopBgm() {
        bgm.stop();
    }
}
