package main.java.com.shooter.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import main.java.com.shooter.Config;
import main.java.com.shooter.GameApp;
import main.java.com.shooter.rest.model.AuthInfo;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;


public class AuthController {

    private static final String AUTH_PREFIX = "auth/";
    private static final String LOGIN_PREFIX = "login";
    private static final String REGISTER_PREFIX = "signup";
    private static final String IMPROPER_INPUT_MESSAGE = "You should fill username field properly!";

    @FXML
    protected TextField username;
    @FXML
    protected PasswordField password;
    @FXML
    protected Label loginRegisterErrorSegment;

    /**
     * Login functionality
     * @param event: Event that triggers the login via login button
     */
    @SuppressWarnings("unused")
    @FXML
    protected void login(ActionEvent event) {
        loginRegisterProcedure(LOGIN_PREFIX);
    }

    /**
     * Registration functionality
     * @param event: Event that triggers the registration via register button
     */
    @SuppressWarnings("unused")
    @FXML
    protected void register(ActionEvent event){
        loginRegisterProcedure(REGISTER_PREFIX);
    }

    /**
     * Private function for login/register procedure not to repeat code
     * @param endpoint: Endpoint for request
     */
    @FXML
    private void loginRegisterProcedure(String endpoint) {
        String currentUsername = username.getText();
        String currentPassword = password.getText();

        if (currentUsername.trim().length() > 0) {
            boolean isSuccess = loginRegisterUser(currentUsername, currentPassword, endpoint);
            ifSuccessLoadGameScene(isSuccess);
        } else {
            showErrorToUser(IMPROPER_INPUT_MESSAGE);
        }
    }


    /**
     * Login/register the user with given username and password
     * @param username: Username of user
     * @param password: Password of user
     * @param endpoint: Endpoint to request
     * @return the operation is successfully completed or not
     */
    private boolean loginRegisterUser(String username, String password, String endpoint) {
        try {
            AuthInfo user = new AuthInfo(username, password);
            String uri = Config.getGameUrl() + AUTH_PREFIX + endpoint;
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<AuthInfo> request = new HttpEntity<>(user);
            Integer pid = restTemplate.postForObject(uri, request, Integer.class);
            user.setId(pid);
            GameController.setPlayerID(pid);
            return true;
        } catch (HttpClientErrorException e) {
            try {
                String errorMessage = (new JSONObject(e.getResponseBodyAsString())).getString("message");
                showErrorToUser(errorMessage);
                return false;
            } catch (JSONException jsonError) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Show given content to label fields under login/register buttons
     * @param errorContent: String content of error
     */
    private void showErrorToUser(String errorContent) {
        loginRegisterErrorSegment.setText(errorContent);
        loginRegisterErrorSegment.setVisible(true);
    }

    /**
     * Loads game inner scene to continue the game
     * @param isSuccess: Whether it is success or not
     */
    private void ifSuccessLoadGameScene(boolean isSuccess) {
        if (isSuccess) {
            try {
                GameApp.setRootToScene(FXMLLoader.load(getClass().getResource(Config.VIEW_START_SCREEN_PATH)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
