package main.java.com.shooter.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import main.java.com.shooter.Config;
import main.java.com.shooter.GameApp;
import main.java.com.shooter.GameFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class EndGameScreenController implements Initializable {

    @FXML
    protected Button continueNextLevelButton;
    @FXML
    protected Label scoreValue;
    @FXML
    protected Label endMessage;

    /**
     * Play next level by leveling up
     */
    @SuppressWarnings("unused")
    @FXML
    private void continueNextLevel() {
        int currentLevel = GameFactory.getInstance().getLevel().getLevelNo();
        GameController gameController = new GameController(currentLevel + 1);
    }

    /**
     * Play current level again
     */
    @SuppressWarnings("unused")
    @FXML
    private void playCurrentLevelAgain() {
        int currentLevel = GameFactory.getInstance().getLevel().getLevelNo();
        GameController gameController = new GameController(currentLevel);
    }

    /**
     * Go back to Start Screen
     * @throws IOException: If the FXML file is not available, throw it
     */
    @FXML
    private void goBackToStartScreen() throws IOException {
        GameApp.setRootToScene(FXMLLoader.load(getClass().getResource(Config.VIEW_START_SCREEN_PATH)));
        GameController.stopBgm();
        GameController.playBgmIntro();
    }

    /**
     * Initialize screen functionality with respect to level and player status
     * @param url: Not used for any purposes
     * @param resourceBundle: Not used for any purposes
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Initialize the button with respect to the current level value
        int currentLevel = GameFactory.getInstance().getLevel().getLevelNo(), score = GameFactory.getInstance().getLevel().getLevelScore();
        boolean isPlayerDead = GameFactory.getInstance().getLevel().isPlayerDead();
        if (currentLevel >= Config.MAX_LEVEL || isPlayerDead) {
            continueNextLevelButton.setDisable(true);
        }
        if (isPlayerDead) {
            endMessage.setText("OHH NO... TRY AGAIN!");
        } else {
            endMessage.setText("CONGRATULATIONS!!!");
        }
        scoreValue.setText(Integer.toString(score));
    }
}
