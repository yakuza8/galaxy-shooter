package main.java.com.shooter.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import main.java.com.shooter.Config;
import main.java.com.shooter.GameApp;
import main.java.com.shooter.rest.model.ScoreAbstract;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

public class LeaderboardController {

    private static final String Score_PREFIX = Config.getGameUrl() + "scores/";
    private static final String AllScore_PREFIX = "all/10";
    private static final String WeeklyScore_PREFIX = "weekly/10";

    @FXML
    protected GridPane grid;

    @FXML
    protected Label allweek;

    /**
     * @param event: Event that triggers leaderboard action via leaderboard button
     * @throws IOException: If the loading screen is not available, then throw it
     */
    @SuppressWarnings("unused")
    @FXML
    protected void goBack(ActionEvent event) throws IOException {
        GameApp.setRootToScene(FXMLLoader.load(getClass().getResource(Config.VIEW_START_SCREEN_PATH)));
    }

    @SuppressWarnings("unused")
    @FXML
    protected void getAllLeaderboard() {
        allweek.setText("ALL TIME");
        getScores(AllScore_PREFIX);
    }

    @SuppressWarnings("unused")
    @FXML
    protected void getWeeklyLeaderboard(ActionEvent event) {
        allweek.setText("WEEKLY");
        getScores(WeeklyScore_PREFIX);
    }

    /**
     * Get scores with respect to given prefix of endpoint
     *
     * @param prefix: Endpoint prefix for request
     */
    @SuppressWarnings("ConstantConditions")
    private void getScores(String prefix) {
        // Get table children
        grid.getChildren().retainAll(grid.getChildren().get(0), grid.getChildren().get(1), grid.getChildren().get(2));
        grid.setAlignment(Pos.CENTER);
        // URI to request
        String uri = Score_PREFIX + prefix;
        RestTemplate restTemplate = new RestTemplate();
        ScoreAbstract[] forObject = restTemplate.getForObject(uri, ScoreAbstract[].class);
        // Fill table
        if (forObject != null) {
            for (int i = 0; i < forObject.length; i++) {
                ScoreAbstract s = forObject[i];
                grid.add(new Label(String.valueOf(i + 1)), 0, i + 1);
                grid.add(new Label(s.getPlayer().getNickname()), 1, i + 1);
                grid.add(new Label(s.getScore().toString()), 2, i + 1);
            }
        }
    }

}
