package main.java.com.shooter.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import main.java.com.shooter.Config;
import main.java.com.shooter.GameApp;

import java.io.IOException;

public class StartScreenController {

    /**
     * Start game event that sets the necessary scene and pane to the stage
     *
     * @param event: Event that trigger the starting operation
     */
    @SuppressWarnings("unused")
    @FXML
    protected void startGame(ActionEvent event) {
        Node node = (Node) event.getSource();
        String data = (String) node.getUserData();
        int value = Integer.parseInt(data);
        GameController gameController = new GameController(value);
    }

    /**
     * @param event: Event that trigger the leaderboard operation
     * @throws IOException: If the leaderboard screen is not available, then throw it
     */
    @SuppressWarnings("unused")
    @FXML
    protected void getLeaderboard(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(Config.VIEW_LEADERBOARD_SCREEN_PATH));
        loader.load();
        GameApp.setRootToScene(loader.getRoot());
        LeaderboardController controller = loader.getController();
        controller.getAllLeaderboard();
    }

}
