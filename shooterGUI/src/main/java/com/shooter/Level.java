package main.java.com.shooter;

import main.java.com.shooter.entity.Enemy;

@SuppressWarnings({"unused", "WeakerAccess"})
public class Level {

    private double enemySpawnChance = Config.ENEMY_SPAWN_CHANCE;
    private double enemySmallSpawnChance = Config.ENEMY_SMALL_SPAWN_CHANCE;
    private double enemyMediumSpawnChance = Config.ENEMY_MEDIUM_SPAWN_CHANCE;
    private double enemyLargeSpawnChance = Config.ENEMY_LARGE_SPAWN_CHANCE;
    private int enemySmallHealth = Config.ENEMY_SMALL_HEALTH;
    private int enemyMediumHealth = Config.ENEMY_MEDIUM_HEALTH;
    private int enemyLargeHealth = Config.ENEMY_LARGE_HEALTH;
    private double enemySmallScore = Config.ENEMY_SMALL_SCORE;
    private double enemyMediumScore = Config.ENEMY_MEDIUM_SCORE;
    private double enemyLargeScore = Config.ENEMY_LARGE_SCORE;
    private double bulletEnemySpawnChance = Config.BULLET_ENEMY_SPAWN_CHANCE_DEFAULT;
    private double levelTime = Config.LEVEL_TIME;
    public double levelTimeDeltaBeforeIncrease = Config.LEVEL_TIME_DELTA_BEFORE_INCREASE;
    public double levelDifficultyDeltaBeforeChange = Config.LEVEL_DIFFICULTY_DELTA_BEFORE_CHANGE;
    private int levelNo = 1;
    private boolean isPlayerDead = false;
    private int levelScore;

    private boolean isGameOngoing;
    private boolean isBossSpawned;
    private Enemy enemyBoss;

    public double getEnemySpawnChance() {
        return enemySpawnChance;
    }

    public Level setEnemySpawnChance(double enemySpawnChance) {
        this.enemySpawnChance = enemySpawnChance;
        return this;
    }

    public double getEnemySmallSpawnChance() {
        return enemySmallSpawnChance;
    }

    public Level setEnemySmallSpawnChance(double enemySmallSpawnChance) {
        this.enemySmallSpawnChance = enemySmallSpawnChance;
        return this;
    }

    public double getEnemyMediumSpawnChance() {
        return enemyMediumSpawnChance;
    }

    public Level setEnemyMediumSpawnChance(double enemyMediumSpawnChance) {
        this.enemyMediumSpawnChance = enemyMediumSpawnChance;
        return this;
    }

    public double getEnemyLargeSpawnChance() {
        return enemyLargeSpawnChance;
    }

    public Level setEnemyLargeSpawnChance(double enemyLargeSpawnChance) {
        this.enemyLargeSpawnChance = enemyLargeSpawnChance;
        return this;
    }

    public int getEnemySmallHealth() {
        return enemySmallHealth;
    }

    public Level setEnemySmallHealth(int enemySmallHealth) {
        this.enemySmallHealth = enemySmallHealth;
        return this;
    }

    public int getEnemyMediumHealth() {
        return enemyMediumHealth;
    }

    public Level setEnemyMediumHealth(int enemyMediumHealth) {
        this.enemyMediumHealth = enemyMediumHealth;
        return this;
    }

    public int getEnemyLargeHealth() {
        return enemyLargeHealth;
    }

    public Level setEnemyLargeHealth(int enemyLargeHealth) {
        this.enemyLargeHealth = enemyLargeHealth;
        return this;
    }

    public double getEnemySmallScore() {
        return enemySmallScore;
    }

    public Level setEnemySmallScore(double enemySmallScore) {
        this.enemySmallScore = enemySmallScore;
        return this;
    }

    public double getEnemyMediumScore() {
        return enemyMediumScore;
    }

    public Level setEnemyMediumScore(double enemyMediumScore) {
        this.enemyMediumScore = enemyMediumScore;
        return this;
    }

    public double getEnemyLargeScore() {
        return enemyLargeScore;
    }

    public Level setEnemyLargeScore(double enemyLargeScore) {
        this.enemyLargeScore = enemyLargeScore;
        return this;
    }

    public double getBulletEnemySpawnChance() {
        return bulletEnemySpawnChance;
    }

    public Level setBulletEnemySpawnChance(double bulletEnemySpawnChance) {
        this.bulletEnemySpawnChance = bulletEnemySpawnChance;
        return this;
    }

    public double getLevelTime() {
        return levelTime;
    }

    public Level setLevelTime(double levelTime) {
        this.levelTime = levelTime;
        return this;
    }

    public int getLevelNo() {
        return levelNo;
    }

    public Level setLevelNo(int levelNo) {
        this.levelNo = levelNo;
        return this;
    }

    public double getLevelTimeDeltaBeforeIncrease() {
        return levelTimeDeltaBeforeIncrease;
    }

    public Level setLevelTimeDeltaBeforeIncrease(double levelTimeDeltaBeforeIncrease) {
        this.levelTimeDeltaBeforeIncrease = levelTimeDeltaBeforeIncrease;
        return this;
    }

    public double getLevelDifficultyDeltaBeforeChange() {
        return levelDifficultyDeltaBeforeChange;
    }

    public Level setLevelDifficultyDeltaBeforeChange(double levelDifficultyDeltaBeforeChange) {
        this.levelDifficultyDeltaBeforeChange = levelDifficultyDeltaBeforeChange;
        return this;
    }

    public boolean isPlayerDead() {
        return isPlayerDead;
    }

    public Level setPlayerDead(boolean playerDead) {
        isPlayerDead = playerDead;
        return this;
    }

    public int getLevelScore() {
        return levelScore;
    }

    public Level setLevelScore(int levelScore) {
        this.levelScore = levelScore;
        return this;
    }

    public boolean isGameOngoing() {
        return isGameOngoing;
    }

    public Level setGameOngoing(boolean gameOngoing) {
        isGameOngoing = gameOngoing;
        return this;
    }

    public boolean isBossSpawned() {
        return isBossSpawned;
    }

    public Level setBossSpawned(boolean bossSpawned) {
        isBossSpawned = bossSpawned;
        return this;
    }

    public Enemy getEnemyBoss() {
        return enemyBoss;
    }

    public void setEnemyBoss(Enemy enemyBoss) {
        this.enemyBoss = enemyBoss;
    }
}
