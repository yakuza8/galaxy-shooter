package main.java.com.shooter;

import java.io.IOException;
import java.util.Properties;

@SuppressWarnings("WeakerAccess")
public class Config {

    public static int getGamePort() {
        Properties properties = new Properties();
        try {
            properties.load(Config.class.getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(properties.getProperty("server.port", "8082"));
    }

    static public final int GAMESERVER_PORT = getGamePort();

    // Player Roles
    public static final int masterRole = 0;
    public static final int slaveRole = 1;
}
