package main.java.com.shooter;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

@SuppressWarnings("WeakerAccess")
public class GameServer {

    static class HandleSession implements Runnable {

        private final Connection player1;
        private final Connection player2;
        private Listener listener1;
        private Listener listener2;

        HandleSession(Connection c1, Connection c2) {
            this.player1 = c1;
            this.player2 = c2;
        }

        @Override
        public void run() {
            // Listener in order for forwarding player1's messages to player2
            listener1 = new Listener() {
                @Override
                public void received (Connection connection, Object object) {
                    if (object instanceof MessageProtocol.Entities) {
                        MessageProtocol.Entities entities = (MessageProtocol.Entities) object;
                        player2.sendUDP(entities);
                    }
                    else if (object instanceof MessageProtocol.PlayersInfo) {
                        MessageProtocol.PlayersInfo playerInfo = (MessageProtocol.PlayersInfo) object;
                        player2.sendUDP(playerInfo);
                    }
                    else if (object instanceof MessageProtocol.EndGame) {
                        MessageProtocol.EndGame endGame = (MessageProtocol.EndGame) object;
                        player2.sendUDP(endGame);
                    }
                }

                @Override
                public void disconnected(Connection connection) {
                    super.disconnected(connection);
                    player2.sendUDP(new MessageProtocol.EndGame());
                    player2.close();
                }
            };
            // Listener in order for forwarding player2's messages to player1
            listener2 = new Listener() {
                @Override
                public void received (Connection connection, Object object) {
                    if (object instanceof MessageProtocol.PlayersInfo) {
                        MessageProtocol.PlayersInfo playerInfo = (MessageProtocol.PlayersInfo) object;
                        player1.sendUDP(playerInfo);
                    }
                }

                @Override
                public void disconnected(Connection connection) {
                    super.disconnected(connection);
                    player1.sendUDP(new MessageProtocol.EndGame());
                    player1.close();
                }
            };

            player1.addListener(listener1);
            player2.addListener(listener2);

            // Set roles (0 for master player - 1 for slave player)
            this.player1.sendUDP(new MessageProtocol.StartGame().setRole(Config.masterRole));
            this.player2.sendUDP(new MessageProtocol.StartGame().setRole(Config.slaveRole));
        }
    }

    public static void main(String[] args) throws IOException {
        Queue<Connection> queue = new LinkedList<>();
        Server server = new Server();
        MessageProtocol.register(server);

        server.addListener(new Listener() {
            @Override
            public void connected(Connection connection) {
                // If new client connects, add him/her to the queue and if there is more clients get them and match
                super.connected(connection);
                queue.add(connection);
                System.out.println(connection.getID());
                if (queue.size() >= 2){
                    new Thread(new HandleSession(queue.poll(), queue.poll())).start();
                }
            }

            @Override
            public void disconnected (Connection connection) {
                // If disconnect, remove from the queue
                super.disconnected(connection);
                queue.remove(connection);
            }

        });
        server.bind(Config.GAMESERVER_PORT, Config.GAMESERVER_PORT);
        server.start();
    }
}