package com.shooter.controller;


import com.shooter.ShooterApplication;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = ShooterApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AuthInfoControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Integer testPlayerid;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        String jsonString = new JSONObject()
                .put("nickname", "testuser")
                .put("password", "testpassword")
                .toString();

        String testPlayerID = mockMvc.perform(post("/auth/signup").contentType(MediaType.APPLICATION_JSON)
                                                      .content(jsonString))
                .andReturn().getResponse().getContentAsString();
        testPlayerid = Integer.valueOf(testPlayerID);
    }

    @After
    public void cleanup() throws Exception {
        mockMvc.perform(delete("/auth/" + testPlayerid.toString()));
    }


    @Test
    public void test1ASignUpAndDeleteSuccess() throws Exception {
        String jsonString = new JSONObject()
                .put("nickname", "testsignup")
                .put("password", "testpassword")
                .toString();

        String signedUpPlayerid = mockMvc.perform(post("/auth/signup").contentType(MediaType.APPLICATION_JSON)
                                                          .content(jsonString))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn().getResponse().getContentAsString();

        mockMvc.perform(delete("/auth/" + signedUpPlayerid))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void test1BSignUpSameNicknameFail() throws Exception {
        String jsonString = new JSONObject()
                .put("nickname", "testuser") // existing nickname
                .put("password", "testpassword")
                .toString();

        mockMvc.perform(post("/auth/signup").contentType(MediaType.APPLICATION_JSON)
                                .content(jsonString))
                .andExpect(status().is4xxClientError())
                .andDo(print());
    }

    // Both nickname and password are required.
    @Test
    public void test1CSignUpInsufficientFieldsFail() throws Exception {
        String jsonString = new JSONObject()
                .put("nickname", "testuser")
                .toString();

        mockMvc.perform(post("/auth/signup").contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().is4xxClientError())
                .andDo(print());
    }

    @Test
    public void test2ALoginSuccess() throws Exception {

        assert (testPlayerid != null);

        String jsonString = new JSONObject()
                .put("nickname", "testuser")
                .put("password", "testpassword")
                .toString();

        mockMvc.perform(post("/auth/login").contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void test2BLoginWrongAuthFail() throws Exception {

        assert (testPlayerid != null);

        String jsonString = new JSONObject()
                .put("nickname", "testuser")
                .put("password", "WRONGtestpassword")
                .toString();

        mockMvc.perform(post("/auth/login").contentType(MediaType.APPLICATION_JSON)
                                .content(jsonString))
                .andExpect(status().is4xxClientError())
                .andDo(print());
    }

    @Test
    public void test3AGetOneAuthSuccess() throws Exception {

        assert (testPlayerid != null);

        mockMvc.perform(get("/auth/" + testPlayerid.toString()))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void test3BGetOneAuthInvalidIdFail() throws Exception {

        mockMvc.perform(get("/auth/" + "-1"))
                .andExpect(status().is4xxClientError())
                .andDo(print());
    }

    @Test
    public void test4AGetAllAuthSuccess() throws Exception {

        mockMvc.perform(get("/auth"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void test5AReplaceAuthInfoNicknameSuccess() throws Exception {

        assert (testPlayerid != null);

        String jsonString = new JSONObject()
                .put("nickname", "testuserCHANGED")
                .put("password", "testpassword")
                .toString();

        mockMvc.perform(put("/auth/" + testPlayerid.toString())
                                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc.perform(post("/auth/login")
                                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void test5BReplaceAuthInfoExistingNicknameFail() throws Exception {

        assert (testPlayerid != null);

        String jsonString = new JSONObject()
                .put("nickname", "testuserEXISTS")
                .put("password", "testpassword")
                .toString();

        // Create another player
        String signedUpPlayerid = mockMvc.perform(post("/auth/signup").contentType(MediaType.APPLICATION_JSON)
                                                          .content(jsonString))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn().getResponse().getContentAsString();

        // Try to change nickname to existing nickname
        mockMvc.perform(put("/auth/" + testPlayerid.toString())
                                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().is4xxClientError())
                .andDo(print());

        mockMvc.perform(delete("/auth/" + signedUpPlayerid))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void test5CReplaceAuthInfoPasswordSuccess() throws Exception {

        assert (testPlayerid != null);

        String jsonString = new JSONObject()
                .put("nickname", "testuser")
                .put("password", "testpasswordCHANGED")
                .toString();

        mockMvc.perform(put("/auth/" + testPlayerid.toString())
                                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk())
                .andDo(print());

        mockMvc.perform(post("/auth/login")
                                .contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk())
                .andDo(print());
    }

//    @MasterTest(value = "Hocam valla calisiyor")

}
