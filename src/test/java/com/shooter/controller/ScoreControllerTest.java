package com.shooter.controller;

import com.shooter.ShooterApplication;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ShooterApplication.class)
@SpringBootTest
public class ScoreControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Integer testPlayerID;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        String jsonString = new JSONObject()
                .put("nickname", "test_score_user")
                .put("password", "test_score_password")
                .toString();

        String savedTestPlayerID = mockMvc.perform(post("/auth/signup").contentType(MediaType.APPLICATION_JSON)
                .content(jsonString))
                .andReturn()
                .getResponse()
                .getContentAsString();
        testPlayerID = Integer.valueOf(savedTestPlayerID);
    }

    @After
    public void cleanup() throws Exception {
        mockMvc.perform(delete("/auth/" + testPlayerID.toString()));
    }

    @Test
    public void Test1AGetAllTopNScoresSuccess() throws Exception {
        Integer limit = 5;
        mockMvc.perform(get("/scores/all/" + limit.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(lessThanOrEqualTo(limit))))
                .andDo(print());
    }

    @Test
    public void Test1BGetAllTopNScoresNegativeLimit() {
        // MockMVC web application context does not support internal exception handling,
        // then we need to use such mechanism
        int limit = -1;
        try {
            mockMvc.perform(get("/scores/all/" + Integer.toString(limit)))
                    .andDo(print())
                    .andReturn();
        } catch (Exception err) {
            Assert.assertTrue(err.getMessage().contains("Limit must be greater than 0"));
        }

    }

    @Test
    public void Test2AGetWeeklyTopNScoresSuccess() throws Exception {
        int limit = 5;
        mockMvc.perform(get("/scores/weekly/" + Integer.toString(limit)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void Test2BGetWeeklyTopNScoresNegativeLimit() {
        // MockMVC web application context does not support internal exception handling,
        // then we need to use such mechanism
        int limit = -1;
        try {
            mockMvc.perform(get("/scores/weekly/" + Integer.toString(limit)))
                    .andDo(print())
                    .andReturn();
        } catch (Exception err) {
            Assert.assertTrue(err.getMessage().contains("Limit must be greater than 0"));
        }

    }

    @Test
    public void Test3AGetAllScoresSuccess() throws Exception {
        mockMvc.perform(get("/scores/all/raw"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void Test4AGetWeeklyScoresSuccess() throws Exception {
        mockMvc.perform(get("/scores/weekly/raw"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void Test5AGetAllScoresSortedSuccess() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/scores/all"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void Test6AGetWeeklyScoresSortedSuccess() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/scores/weekly"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void Test7AAddNewScoreSuccess() throws Exception {
        assert (testPlayerID != null);

        String jsonString = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "999")
                .toString();

        mockMvc.perform(post("/scores").contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void Test7BAddNewScoreNegativeScoreFail() throws Exception {
        String jsonString = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "-1")
                .toString();

        mockMvc.perform(post("/scores").contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void Test7CAddNewScoreNonExistingPlayerFail() {
        try {
            String jsonString = new JSONObject()
                    .put("player_id", "-1")
                    .put("score", "999")
                    .toString();

            mockMvc.perform(post("/scores").contentType(MediaType.APPLICATION_JSON).content(jsonString))
                    .andDo(print());
        } catch (Exception err) {
            Assert.assertTrue(err.getMessage().contains("ConstraintViolationException"));
        }
    }

    @Test
    public void Test8AAddNewAllScoreSuccess() throws Exception {
        assert (testPlayerID != null);

        String jsonString = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "999")
                .toString();

        mockMvc.perform(post("/scores/all").contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void Test8BAddNewAllScoreNegativeScoreFail() throws Exception {
        String jsonString = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "-1")
                .toString();

        mockMvc.perform(post("/scores/all").contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void Test8CAddNewAllScoreNonExistingPlayerFail() {
        try {
            String jsonString = new JSONObject()
                    .put("player_id", "-1")
                    .put("score", "999")
                    .toString();

            mockMvc.perform(post("/scores/all").contentType(MediaType.APPLICATION_JSON).content(jsonString))
                    .andDo(print());
        } catch (Exception err) {
            Assert.assertTrue(err.getMessage().contains("ConstraintViolationException"));
        }
    }

    @Test
    public void Test9AAddNewWeeklyScoreSuccess() throws Exception {
        assert (testPlayerID != null);

        String jsonString = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "999")
                .toString();

        mockMvc.perform(post("/scores/weekly").contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void Test9BAddNewWeeklyScoreNegativeScoreFail() throws Exception {
        String jsonString = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "-1")
                .toString();

        mockMvc.perform(post("/scores/weekly").contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void Test9CAddNewWeeklyScoreNonExistingPlayerFail() {
        try {
            String jsonString = new JSONObject()
                    .put("player_id", "-1")
                    .put("score", "999")
                    .toString();

            mockMvc.perform(post("/scores/weekly").contentType(MediaType.APPLICATION_JSON).content(jsonString))
                    .andDo(print());
        } catch (Exception err) {
            Assert.assertTrue(err.getMessage().contains("ConstraintViolationException"));
        }
    }

    private int saveAndReturnID(Integer testPlayerID, String path) throws Exception {
        String jsonString = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "999")
                .toString();

        String result = mockMvc.perform(post("/scores/" + path).contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        return (new JSONObject(result)).getInt("score_id");
    }

    @Test
    public void Test10AReplaceScoreAllSuccess() throws Exception {
        assert (testPlayerID != null);

        int savedRecordID = saveAndReturnID(testPlayerID, ScoreController.ALL);

        String jsonString2 = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "1000")
                .toString();

        mockMvc.perform(put("/scores/all/" + Integer.toString(savedRecordID)).contentType(MediaType.APPLICATION_JSON).content(jsonString2))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void Test10BReplaceScoreAllNonExistingScoreFail() throws Exception {
        assert (testPlayerID != null);

        String jsonString2 = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "1000")
                .toString();

        mockMvc.perform(put("/scores/all/-1").contentType(MediaType.APPLICATION_JSON).content(jsonString2))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void Test10CReplaceScoreAllNonExistingPlayerFail() {
        try {
            assert (testPlayerID != null);

            int savedRecordID = saveAndReturnID(testPlayerID, ScoreController.ALL);

            String jsonString2 = new JSONObject()
                    .put("player_id", -1)
                    .put("score", "1000")
                    .toString();

            mockMvc.perform(put("/scores/all/" + Integer.toString(savedRecordID)).contentType(MediaType.APPLICATION_JSON).content(jsonString2))
                    .andExpect(status().is4xxClientError())
                    .andDo(print());
        }
        catch (Exception err) {
            Assert.assertTrue(err.getMessage().contains("ConstraintViolationException"));
        }
    }

    @Test
    public void Test10DReplaceScoreAllNegativeScoreFail() throws Exception {
        assert (testPlayerID != null);

        int savedRecordID = saveAndReturnID(testPlayerID, ScoreController.ALL);

        String jsonString2 = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "-1")
                .toString();

        mockMvc.perform(put("/scores/all/" + Integer.toString(savedRecordID)).contentType(MediaType.APPLICATION_JSON).content(jsonString2))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void Test11AReplaceScoreWeeklySuccess() throws Exception {
        assert (testPlayerID != null);

        int savedRecordID = saveAndReturnID(testPlayerID, ScoreController.WEEKLY);

        String jsonString2 = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "1000")
                .toString();

        mockMvc.perform(put("/scores/weekly/" + Integer.toString(savedRecordID)).contentType(MediaType.APPLICATION_JSON).content(jsonString2))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void Test11BReplaceScoreWeeklyNonExistingScoreFail() throws Exception {
        assert (testPlayerID != null);

        String jsonString2 = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "1000")
                .toString();

        mockMvc.perform(put("/scores/weekly/-1").contentType(MediaType.APPLICATION_JSON).content(jsonString2))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    public void Test11CReplaceScoreWeeklyNonExistingPlayerFail() {
        try {
            assert (testPlayerID != null);

            int savedRecordID = saveAndReturnID(testPlayerID, ScoreController.WEEKLY);

            String jsonString2 = new JSONObject()
                    .put("player_id", -1)
                    .put("score", "1000")
                    .toString();

            mockMvc.perform(put("/scores/weekly/" + Integer.toString(savedRecordID)).contentType(MediaType.APPLICATION_JSON).content(jsonString2))
                    .andExpect(status().is4xxClientError())
                    .andDo(print());
        }
        catch (Exception err) {
            Assert.assertTrue(err.getMessage().contains("ConstraintViolationException"));
        }
    }

    @Test
    public void Test11DReplaceScoreWeeklyNegativeScoreFail() throws Exception {
        assert (testPlayerID != null);

        int savedRecordID = saveAndReturnID(testPlayerID, ScoreController.WEEKLY);

        String jsonString2 = new JSONObject()
                .put("player_id", testPlayerID.toString())
                .put("score", "-1")
                .toString();

        mockMvc.perform(put("/scores/weekly/" + Integer.toString(savedRecordID)).contentType(MediaType.APPLICATION_JSON).content(jsonString2))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void Test12ADeleteScoreAllSuccess() throws Exception {
        assert (testPlayerID != null);

        int savedRecordID = saveAndReturnID(testPlayerID, ScoreController.ALL);

        mockMvc.perform(delete("/scores/all/" + Integer.toString(savedRecordID)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void Test12BDeleteScoreAllNonExistingScoreFail() {
        try {
            assert (testPlayerID != null);

            mockMvc.perform(delete("/scores/all/-1"))
                    .andExpect(status().isOk())
                    .andDo(print());
        } catch (Exception err) {
            Assert.assertTrue(err.getMessage().contains("EmptyResultDataAccessException"));
        }
    }

    @Test
    public void Test12CDeleteScoreWeeklySuccess() throws Exception {
        assert (testPlayerID != null);

        int savedRecordID = saveAndReturnID(testPlayerID, ScoreController.WEEKLY);

        mockMvc.perform(delete("/scores/weekly/" + Integer.toString(savedRecordID)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void Test12DDeleteScoreWeeklyNonExistingScoreFail() {
        try {
            assert (testPlayerID != null);

            mockMvc.perform(delete("/scores/weekly/-1"))
                    .andExpect(status().isOk())
                    .andDo(print());
        } catch (Exception err) {
            Assert.assertTrue(err.getMessage().contains("EmptyResultDataAccessException"));
        }
    }

}
