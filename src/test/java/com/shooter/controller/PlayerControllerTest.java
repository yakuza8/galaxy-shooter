package com.shooter.controller;

import com.shooter.ShooterApplication;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = ShooterApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PlayerControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Integer testPlayerid;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        String jsonString = new JSONObject()
                .put("nickname", "testuser")
                .put("password", "testpassword")
                .toString();

        String testPlayerID = mockMvc.perform(post("/auth/signup").contentType(MediaType.APPLICATION_JSON)
                .content(jsonString))
                .andReturn()
                .getResponse()
                .getContentAsString();
        testPlayerid = Integer.valueOf(testPlayerID);
    }

    @After
    public void cleanup() throws Exception {
        mockMvc.perform(delete("/auth/" + testPlayerid.toString()));
    }


    @Test
    public void test1AGetAllPlayersFunctionalityTest() throws Exception {
        mockMvc.perform(get("/players"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void test1BGetOnePlayerWithValidIdTest() throws Exception {
        mockMvc.perform(get("/players/" + testPlayerid.toString()))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void test2AGetOnePlayerNonExistingPlayerTest() throws Exception {
        mockMvc.perform(get("/players/-1"))
                .andExpect(status().is4xxClientError())
                .andDo(print());
    }


}
