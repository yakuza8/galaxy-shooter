package com.shooter.controller;

import com.shooter.entity.Player;
import com.shooter.exception.PlayerNotFoundException;
import com.shooter.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("ALL")
@RestController
public class PlayerController {

    @Autowired
    private PlayerRepository playerRepository;

    /**
     * Method to achieve getting all player records
     * @return All players exist in the database
     */
    @GetMapping("/players")
    List<Player> getAllPlayers() {
        return playerRepository.findAll();
    }

    /**
     * Method to achieve getting a player record with the given id
     * @param id
     * @return Found player record
     * @throws PlayerNotFoundException: throws when the requested player with given id does not exist
     */
    @GetMapping("/players/{id}")
    Player getOnePlayer(@PathVariable Integer id) throws PlayerNotFoundException {
        Optional<Player> possiblePlayer = playerRepository.findById(id);
        if (possiblePlayer.isPresent()) {
            return possiblePlayer.get();
        } else {
            throw new PlayerNotFoundException();
        }
    }

}
