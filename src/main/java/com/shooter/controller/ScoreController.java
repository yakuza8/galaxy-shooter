package com.shooter.controller;

import com.shooter.entity.ScoreAbstract;
import com.shooter.entity.ScoreAll;
import com.shooter.entity.ScoreWeekly;
import com.shooter.exception.InvalidScoreValueException;
import com.shooter.exception.ScoreNotFoundException;
import com.shooter.repository.ScoreAllRepository;
import com.shooter.repository.ScoreWeeklyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@SuppressWarnings("ALL")
@EnableScheduling
@RestController
public class ScoreController {

    public static final String ALL = "all";
    public static final String WEEKLY = "weekly";

    @Autowired
    private ScoreAllRepository scoreAllRepository;

    @Autowired
    private ScoreWeeklyRepository scoreWeeklyRepository;

    /**
     * Method to achieve getting top most N scores with respect to all the time scores
     * @param limit
     * @return Maximum weekly scores with the given limit
     * @throws IndexOutOfBoundsException: throws when limit <= 0
     */
    @GetMapping("/scores/all/{limit}")
    List<ScoreAll> getAllTopNScores(@PathVariable Integer limit) throws IndexOutOfBoundsException {
        try {
            return scoreAllRepository.findByOrderByScoreDesc(PageRequest.of(0, limit));
        } catch (Exception e) {
            throw new IndexOutOfBoundsException("Limit must be greater than 0");
        }
    }

    /**
     * Method to achieve getting top most N scores with respect to weekly scores
     * @param limit
     * @return Maximum weekly scores with the given limit
     * @throws IndexOutOfBoundsException: throws when limit <= 0
     */
    @GetMapping("/scores/weekly/{limit}")
    List<ScoreWeekly> getWeeklyTopNScores(@PathVariable Integer limit) throws IndexOutOfBoundsException {
        try {
            return scoreWeeklyRepository.findByOrderByScoreDesc(PageRequest.of(0, limit));
        } catch (Exception e) {
            throw new IndexOutOfBoundsException("Limit must be greater than 0");
        }
    }

    /**
     * Method to achieve getting all of all the time scores as in the database
     * @return Whole portion of all the time scores
     */
    @GetMapping("/scores/all/raw")
    List<ScoreAll> getAllScores() {
        return scoreAllRepository.findAll();
    }

    /**
     * Method to achieve getting all of weekly scores as in the database
     * @return Whole portion of weekly scores
     */
    @GetMapping("/scores/weekly/raw")
    List<ScoreWeekly> getWeeklyScores() {
        return scoreWeeklyRepository.findAll();
    }

    /**
     * Method to achieve getting all of all the time scores
     * as descending ordered by score and ascending ordered by date
     * @return Whole portion of all the time scores as sorted
     */
    @GetMapping("/scores/all")
    List<ScoreAll> getAllScoresSorted() {
        return scoreAllRepository.findAllByOrderByScoreDescDateAsc();
    }

    /**
     * Method to achieve getting all of weekly scores
     * as descending ordered by score and ascending ordered by date
     * @return Whole portion of weekly scores as sorted
     */
    @GetMapping("/scores/weekly")
    List<ScoreWeekly> getWeeklyScoresSorted() {
        return scoreWeeklyRepository.findAllByOrderByScoreDescDateAsc();
    }

    /**
     * Method to achieve checking score has negative value or not and sets date if it is not available
     * @param newScore
     * @throws InvalidScoreValueException
     */
    private void checkScoreAndSetDate(ScoreAbstract newScore) throws InvalidScoreValueException {
        if (newScore.getScore() < 0) {
            throw new InvalidScoreValueException();
        }
        if (newScore.getDate() == null) {
            Date date = new Date();
            newScore.setDate(date);
        }
    }

    /**
     * Method to achieve adding new score to database tables both all the time score table and weekly score table
     * @param newScore
     * @return Saved ScoreAll instance (it is our selection, it can be vice versa)
     * @throws InvalidScoreValueException: throws when score < 0
     */
    @PostMapping("/scores")
    ScoreAll addNewScore(@RequestBody ScoreAll newScore) throws InvalidScoreValueException {
        checkScoreAndSetDate(newScore);
        ScoreWeekly weeklyScoreToBeSaved = new ScoreWeekly(newScore);
        scoreWeeklyRepository.save(weeklyScoreToBeSaved);
        return scoreAllRepository.save(newScore);
    }

    /*
      Alternative method to achieve adding new score to database tables both all the time score table and weekly score table
      @return Saved ScoreAll instance (it is our selection, it can be vice versa)
     */
    /*@PostMapping("/scores/add2")
    ScoreAll addNewScore2(@RequestParam(name = "player_id") Integer pid, @RequestParam(name = "score") Integer sc) {
        ScoreAll newScore = new ScoreAll();
        newScore.setPlayer_id(pid);
        newScore.setScore(sc);
        if (newScore.getDate() == null) {
            Date date = new Date();
            newScore.setDate(date);
        }
        ScoreWeekly weeklyScoreToBeSaved = new ScoreWeekly(newScore);
        scoreWeeklyRepository.save(weeklyScoreToBeSaved);
        return scoreAllRepository.save(newScore);
    }*/

    /**
     * Method to achieve adding new score to just all the time score table
     * @param newScore
     * @return Saved ScoreAll instance
     */
    @PostMapping("/scores/all")
    ScoreAll addNewAllScore(@RequestBody ScoreAll newScore) {
        checkScoreAndSetDate(newScore);
        return scoreAllRepository.save(newScore);
    }

    /**
     * Method to achieve adding new score to just weekly score table
     * @param newScore
     * @return Saved ScoreWeekly instance
     */
    @PostMapping("/scores/weekly")
    ScoreWeekly addNewWeeklyScore(@RequestBody ScoreWeekly newScore) {
        checkScoreAndSetDate(newScore);
        return scoreWeeklyRepository.save(newScore);
    }

    /**
     * Helper method to achieve updating Score instance and preventing code duplication
     * @param score
     * @param newScore
     */
    private void updateScoreParameters(ScoreAbstract score, ScoreAbstract newScore) {
        checkScoreAndSetDate(newScore);
        score.setDate(newScore.getDate());
        score.setScore(newScore.getScore());
        score.setPlayer_id(newScore.getPlayer_id());
    }

    /**
     * Method to achieve updating all the time score information
     * @param newScore
     * @param id
     * @return Updated record id
     */
    @PutMapping("/scores/all/{id}")
    Integer replaceScoreAll(@RequestBody ScoreAll newScore, @PathVariable Integer id) {
        return scoreAllRepository.findById(id)
            .map(scoreAll -> {
                updateScoreParameters(scoreAll, newScore);
                scoreAllRepository.save(scoreAll);
                return scoreAll.getScore_id();
            })
            .orElseGet(() -> {
                throw new ScoreNotFoundException(ALL);
            });
    }

    /**
     * Method to achieve updating weekly score information
     * @param newScore
     * @param id
     * @return Updated record id
     */
    @PutMapping("/scores/weekly/{id}")
    Integer replaceScoreWeekly(@RequestBody ScoreAll newScore, @PathVariable Integer id) {
        return scoreWeeklyRepository.findById(id)
            .map(scoreWeekly -> {
                updateScoreParameters(scoreWeekly, newScore);
                scoreWeeklyRepository.save(scoreWeekly);
                return scoreWeekly.getScore_id();
            })
            .orElseGet(() -> {
                throw new ScoreNotFoundException(WEEKLY);
            });
    }

    /**
     * Method to delete all the time score with the given id
     * @param id
     */
    @DeleteMapping("/scores/all/{id}")
    void deleteScoreAll(@PathVariable Integer id) {
        scoreAllRepository.deleteById(id);
    }

    /**
     * Method to delete weekly score with the given id
     * @param id
     */
    @DeleteMapping("/scores/weekly/{id}")
    void deleteScoreWeekly(@PathVariable Integer id) {
        scoreWeeklyRepository.deleteById(id);
    }

    /**
     * Scheduled event such that it deletes expired records from weekly scores
     * Fires at minutes which are multiple of 15 minutes
     */
    // @Scheduled(cron = "*/15 * * * * *")
    @Scheduled(fixedRate = 15 * 1000)
    void deleteExpiredScores(){
        // LocalDateTime expiryDate = LocalDateTime.now().minusDays(7);
        Date expiryDate = new Date();
        expiryDate.setTime(expiryDate.getTime() - (long) 7*24*3600*1000);
        scoreWeeklyRepository.deleteAllByDateBefore(expiryDate);
    }

    /*@Deprecated
    @GetMapping("/scores/lastweek")
    List<ScoreAll> getAllScoresFromOneWeekBefore() {
        Date endDate = new Date();
        Long startDateTime = (endDate.getTime()) - (7 * 24 * 3600 * 1000);
        Date startDate = new Date(startDateTime);
        return scoreAllRepository.findAllByDateIsBetween(startDate, endDate);
    }*/

    /*@Deprecated
    @GetMapping("/scores/summed")
    List<Object> getSummedScores() {
        return scoreAllRepository.findTopPlayers();
    }*/

}
