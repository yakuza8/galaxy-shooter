package com.shooter.controller;

import com.shooter.entity.AuthInfo;
import com.shooter.exception.InvalidPlayerInformation;
import com.shooter.exception.PlayerAlreadyExistsException;
import com.shooter.exception.PlayerNotFoundException;
import com.shooter.repository.AuthInfoRepository;
import com.shooter.utils.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;

@SuppressWarnings({"ALL", "CaughtExceptionImmediatelyRethrown"})
@RestController
public class AuthInfoController {

    @Autowired
    private AuthInfoRepository authInfoRepository;

    /**
     * Method to achieve getting all authentication records
     * From security point of view, this is not secure to share, so this endpoint should not be functioned
     * or it may be reached in a secure manner with role based authentication
     * @return All the authentication information stored in DB
     */
    @GetMapping("/auth")
    List<AuthInfo> getAllAuthInfo() {
        return authInfoRepository.findAll();
    }

    /**
     * Method to achieve saving new players
     * @param newAuthInfo
     * @return New record's player id
     * @throws NoSuchAlgorithmException: throws when security library is not installed properly
     */
    @PostMapping("/auth/signup")
    Integer addNewAuthInfo(@RequestBody AuthInfo newAuthInfo) throws NoSuchAlgorithmException {
        if (newAuthInfo.getNickname() == null || newAuthInfo.getPassword() == null) {
            throw new InvalidPlayerInformation();
        }
        try {
            // Create salt to be used while hashing, then store the password not as plaintext
            String salt = new SecureRandom().toString();
            String hashedPassword = PasswordEncoder.encode(newAuthInfo.getPassword(), salt);
            newAuthInfo.setPassword(hashedPassword);
            newAuthInfo.setSalt(salt);
            authInfoRepository.save(newAuthInfo);
        } catch (NoSuchAlgorithmException ex) {
            throw ex;
        } catch (DataIntegrityViolationException e) {
            throw new PlayerAlreadyExistsException();
        }
        return newAuthInfo.getId();
    }

    /**
     * Method to achieve checking specific records in the system (but it should be done in a controlled way)
     * @param id
     * @return Record with given id
     * @throws PlayerNotFoundException: throws when no record exists with the given id
     */
    @GetMapping("/auth/{id}")
    AuthInfo getOnePlayer(@PathVariable Integer id) throws PlayerNotFoundException {
        Optional<AuthInfo> possibleAuthInfo = authInfoRepository.findById(id);
        if (possibleAuthInfo.isPresent()) {
            return possibleAuthInfo.get();
        } else {
            throw new PlayerNotFoundException(id);
        }
    }

    /**
     * Method to achieve updating player information
     * @param newAuthInfo
     * @param id
     * @return Updated record id
     */
    @PutMapping("/auth/{id}")
    Integer replaceAuthInfo(@RequestBody AuthInfo newAuthInfo, @PathVariable Integer id) {
        return authInfoRepository.findById(id)
            .map(player -> {
                try {
                    // Re-set players nickname and password
                    player.setNickname(newAuthInfo.getNickname());
                    player.setPassword(PasswordEncoder.encode(newAuthInfo.getPassword(), player.getSalt()));
                    authInfoRepository.save(player);
                    return player.getId();
                } catch (Exception e){
                    e.printStackTrace();
                    return newAuthInfo.getId();
                }
            })
            .orElseGet(() -> {
                throw new PlayerNotFoundException(id);
            });
    }

    /**
     * Method to achieve deleting the user with given id
     * @param id
     */
    @DeleteMapping("/auth/{id}")
    void deleteAuthInfo(@PathVariable Integer id) {
        authInfoRepository.deleteById(id);
    }

    /**
     * Method to achieve logging the player into the game
     * @param authInfo
     * @return Player id of signed in player
     * @throws InvalidPlayerInformation: throws when the given password and nickname match does not hold
     * @throws PlayerNotFoundException: throws when given nickname does not exist in the database
     * @throws NoSuchAlgorithmException: throws when password encoder does not exist at the server side
     */
    @PostMapping("auth/login")
    Integer loginPlayer(@RequestBody AuthInfo authInfo) throws InvalidPlayerInformation, PlayerNotFoundException, NoSuchAlgorithmException {
        //noinspection CaughtExceptionImmediatelyRethrown
        try {
            AuthInfo foundAuthInfo = authInfoRepository.findByNickname(authInfo.getNickname());
            if (foundAuthInfo != null) {
                String hashedPassword = PasswordEncoder.encode(authInfo.getPassword(), foundAuthInfo.getSalt());
                if (hashedPassword.equals(foundAuthInfo.getPassword())) {
                    return foundAuthInfo.getId();
                } else {
                    throw new InvalidPlayerInformation();
                }
            } else {
                throw new PlayerNotFoundException();
            }
        } catch (NoSuchAlgorithmException e) {
            throw e;
        }
    }
}
