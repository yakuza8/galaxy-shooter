package com.shooter.entity;

import javax.persistence.*;
import java.util.Date;


@SuppressWarnings("unused")
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class ScoreAbstract {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer score_id;

    @Column(name = "score_player_id")
    private Integer player_id;

    @Column(name = "score_score")
    private Integer score;

    @Column(name = "score_date", columnDefinition = "TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @ManyToOne
    @JoinColumn(name = "score_player_id", referencedColumnName = "player_id", insertable = false, updatable = false)
    private Player player;

    public Integer getScore_id() {
        return score_id;
    }

    public void setScore_id(Integer score_id) {
        this.score_id = score_id;
    }

    public Integer getPlayer_id() {
        return player_id;
    }

    public void setPlayer_id(Integer player_id) {
        this.player_id = player_id;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public String toString() {
        return "ScoreAbstract{" +
                "score_id=" + score_id +
                ", player_id=" + player_id +
                ", score=" + score +
                ", date=" + date +
                ", player=" + player +
                '}';
    }
}
