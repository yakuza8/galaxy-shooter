package com.shooter.entity;

import javax.persistence.*;

/**
 * Class for joining Score entities to join
 * Without Player class, when we request scores, secret information like passwords come together
 * To prevent the situation, wrap the necessary part of it with player
 */
@SuppressWarnings("ALL")
@Entity
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "player_id")
    private Integer id;

    @Column(name = "player_nickname", unique = true)
    private String nickname;

    /* No needed for score joining, but keep it as reminder
    @OneToMany
    private List<ScoreAll> scoreAll;

    @OneToMany(mappedBy = "scoreWeekly")
    private List<ScoreWeekly> scoreWeekly;
    */

    /* Can be used with { @link findTopPlayers } located in ScoreAllRepository as alternative leaderboard
    public Integer getSumScoreAll() {
        Integer sum = 0;
        for (ScoreAll score : scoreAll) {
            sum += score.getScore();
        }
        return sum;
    }*/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
