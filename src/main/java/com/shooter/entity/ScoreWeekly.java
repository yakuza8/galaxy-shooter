package com.shooter.entity;

import javax.persistence.*;

@SuppressWarnings("unused")
@Entity
@Table(name = "ScoreWeekly")
public class ScoreWeekly extends ScoreAbstract {

    public ScoreWeekly() {
        super();
    }

    public ScoreWeekly(ScoreAll scoreAll) {
        super();
        this.setPlayer_id(scoreAll.getPlayer_id());
        this.setScore(scoreAll.getScore());
    }

}
