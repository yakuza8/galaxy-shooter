package com.shooter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class InvalidPlayerInformation extends RuntimeException {
    public InvalidPlayerInformation() {
        super("Could not match the given player with given password!");
    }
}
