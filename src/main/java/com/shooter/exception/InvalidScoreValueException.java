package com.shooter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidScoreValueException extends RuntimeException {

    public InvalidScoreValueException() {
        super("Invalid score value, score must be greater than or equal to 0!");
    }

}