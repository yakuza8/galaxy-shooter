package com.shooter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("unused")
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ScoreNotFoundException extends RuntimeException {

    public ScoreNotFoundException() {
        super("Could not find requested score!");
    }

    public ScoreNotFoundException(Integer id) {
        super("Could not find score " + id + "!");
    }

    public ScoreNotFoundException(String type) {
        super("Could not find requested " + type + " score!");
    }
}