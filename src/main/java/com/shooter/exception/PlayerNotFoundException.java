package com.shooter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PlayerNotFoundException extends RuntimeException {

    public PlayerNotFoundException() {
        super("Could not find requested player!");
    }

    public PlayerNotFoundException(Integer id) {
        super("Could not find player " + id + "!");
    }
}
