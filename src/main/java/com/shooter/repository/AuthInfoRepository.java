package com.shooter.repository;

import com.shooter.entity.AuthInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthInfoRepository extends JpaRepository<AuthInfo, Integer> {

    AuthInfo findByNickname(String nickname);

}
