package com.shooter.repository;

import com.shooter.entity.ScoreWeekly;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface ScoreWeeklyRepository extends JpaRepository<ScoreWeekly, Integer> {

    List<ScoreWeekly> findByOrderByScoreDesc(Pageable pageable);

    List<ScoreWeekly> findAllByOrderByScoreDescDateAsc();

    @Transactional
    @Modifying
    void deleteAllByDateBefore(Date expiryDate);
}
