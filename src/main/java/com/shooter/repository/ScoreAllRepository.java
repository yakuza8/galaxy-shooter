package com.shooter.repository;

import com.shooter.entity.ScoreAll;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
public interface ScoreAllRepository extends JpaRepository<ScoreAll, Integer> {

    List<ScoreAll> findByOrderByScoreDesc(Pageable pageable);

    List<ScoreAll> findAllByDateIsBetween(Date startDate, Date endDate);

    List<ScoreAll> findAllByOrderByScoreDescDateAsc();

    // It can be used as alternative leaderboard mechanism
    @Query(value = "SELECT p.player_id, p.player_nickname, sum(s.score_score) AS total_score " +
            "FROM player p INNER JOIN score_all s " +
            "ON p.player_id = s.score_player_id " +
            "GROUP BY p.player_id, p.player_nickname " +
            "ORDER BY total_score DESC", nativeQuery = true)
    List<Object> findTopPlayers();


    List<ScoreAll> countScoreAllByPlayer_id(Integer player_id);

}
