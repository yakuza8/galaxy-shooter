# Galaxy Shooter

This project belongs to following project members
- Berat Cankar
- Murat Coşar

## How to play Galaxy Shooter

Galaxy Shooter is a `shoot'em up` type game such that you operate a spacecraft in our deeply
void space with self shooted infinite ammunition. 

### Controls

You can control the spacecraft with mouse movements in a way that when you push the mouse, it
goes upward in the screen and pull down it goes downward in the screen, and it has the same
procedure in left and right movements.

<img align="left" width="256" height="256" src="https://cdn4.iconfinder.com/data/icons/pc-mouse/154/mouse-control-left-right-top-down-arrows-move-256.png">
As you can see basic controls are given in this scheme.

Currently, your spacecraft automatically fires. However, you can disable this feature by right click.

### Description

In Galaxy Shooter, there are three single player level and one multi player level that you can
play. At the end of each level, your score will be saved by the system. You can see the highscores
from the link that you can see at MainScreen of Galaxy Shooter (after successfully login operation)

You will have `10 health points` and infinite ammunition to shoot your alien enemies

There are 4 types of enemy that you will encounter. They are respectively,

<img align="right" width="275" height="350" src="https://cdn.pbrd.co/images/HS2Hg9V.png">

*  Small enemy  : Don't deceive its cute greenish appearance. Even if it has `1 health point`, it will be the main
enemy type that you will see all the time. 
*  Medium enemy : It has `2 health points` and it has secret weapon prepared for you. It will goes
leftward and rightward periodically with it octopus like tentacles in order not to shoot them easily.
*  Large enemy  : Aaaaand here is a tank enemy of our game it has `3 health points` and more speed than
the other enemies. Don't get angry if it kills you since it is not its choice but its mission :(.
*  Boss enemy: It is just mystery. No one came a cross with him, but with respect to some legends, if you go inside of
dark space with one of your friends (multi player level), maybe you can see him and fight with him. 
If you do so, can you inform us about his properties?

There are currently 3 single player levels in Galaxy Shooter, and also one multi player level where you can encounter the boss.
As you pass levels, the creation probability of enemies and level time to complete also increases.

There are two types upgrades. The first one is +1 HP (Health Point) and the other one is +1 Fire Rate upgrade.
They are randomly created by killing enemies with low chance (Chance increases with levels).

The start screen that contains these information can be seen as well. Resources used for the images can be obtained at https://www.deviantart.com/gooperblooper22/art/Space-Invaders-Sprite-Sheet-135338373 .

Important Note : Please check the correspondence of endpoint prefix of GUI and deployed server path. Path can be changed in the application.properties file.

### User Test Cases

Test Case | Steps | Expected Result
|:-------:|-----|----
|  UTC01  | Start the game | Game start screen appears with `Username` and `Password` text fields also there are two buttons `Login` and `Register`. Back |
|  UTC02  | Login with proper user information like username `test` and password `test`| Game main screen appears with game information fields, leaderboard button under that and level choice box at the bottom of screen |
|  UTC03  | Leave username empty or type just space characters, then try to login | Error appears in the start screen as `You should fill username field properly!`|
|  UTC04  | Try to login with invalid username | Error appears in the start screen as `Could not find requested player!` |
|  UTC05  | Try to login with valid username but incorrect password | Error appears in the start screen as `Could not match the given player with given password!` |
|  UTC06  | Register with valid username and password | Game main screen appears with game information fields, leaderboard button under that and level choice box at the bottom of screen |
|  UTC07  | Try to register with already existing username | Error appears in the start screen as `Given player already exists!` |
|  UTC08  | Click leaderboard button in the main screen | You should get All the Time scores as default |
|  UTC09  | Click leaderboard button in the main screen, then click `GET WEEKLY SCORES` | Appeared table should be updated with weekly scores if exist |
|  UTC10  | Click leaderboard button in the main screen, then click `GET WEEKLY SCORES`, then click `GET ALL SCORES` | Appeared table should be updated with firstly weekly scores, then all the time scores if exist |
|  UTC11  | Click leaderboard button in the main screen, then, click `BACK` | You should return the Main Screen of the game |
|  UTC12  | Select any level from the bottom of Main Screen | Level should start with new background and new sound. Also, there should be score at top left of screen, remaining time in second at top right of screen and health bar at bottom left of screen |
|  UTC13  | While playing level, die before time reaches to 0 | Game End Screen should appear where there are 3 lines of text `OHH NO... TRY AGAIN`, `YOUR SCORE:` and score value, and there should also be 3 buttons respectively `PLAY AGAIN`, `CONTINUE` and `BACK`. `CONTINUE` button should be disabled |
|  UTC14  | While playing level, win the game i.e. time reaches to 0 | Game End Screen should appear where there are 3 lines of text `OHH NO... TRY AGAIN`, `YOUR SCORE:` and score value, and there should also be 3 buttons respectively `PLAY AGAIN`, `CONTINUE` and `BACK`. `CONTINUE` button should be enabled |
|  UTC15  | In the Game End Screen (that comes after completing any level), click `BACK` button | Main Screen appears |
|  UTC16  | In the Game End Screen (that comes after completing any level), click `CONTINUE` button | If current level is less than 3, then next level should start |
|  UTC17  | In the Game End Screen (that comes after completing any level), click `CONTINUE` button when the current level is equal to 3 | Since `CONTINUE` button is disabled, you should not get any reaction |
|  UTC18  | In the Game End Screen (that comes after completing any level), click `CONTINUE` button when you die in any level i.e. before time reaches to 0 | Since `CONTINUE` button is disabled, you should not get any reaction |
|  UTC19  | While playing game, take +1 Health Point upgrade | You should see an increase in your health bar |
|  UTC20  | While playing game, take +1 Fire Rate upgrade | You should observe a decrease time duration between contiguous shoots |
|  UTC21  | While playing game, collide to any enemy | You should observe a decrease in your health bar by the collided enemy's remaining health value |
|  UTC22  | While playing game, try to exceed screen | The spacecraft should not exceed current screen |
|  UTC23  | Any time in the game, try to expand screen size | No reaction should performed since we fixed screen size |
|  UTC24  | In the main screen, choose multi player level | You should wait until an opponent enters you session |
|  UTC25  | In the main screen, choose multi player level, and click `CANCEL` button | You should return the game start screen |
|  UTC26  | While playing multi player level, when time the finished | The boss should be spawned |
|  UTC27  | While playing multi player level, if both player dies | End screen of level should be appear |
|  UTC28  | While playing multi player level, if one of player dies | Game should continue as it is |

* UTC: User Test Case
* From UTC08 to UTC28 test cases, it is needed to perform proper login/register operation as prerequisite.  

## Game Server Documentation

### Description

There is a match maker and central game server whose responsibility is the followings:

*  Initial match making such that when players select multi player level, then the server puts them to the queue and matchs them,
then it conveys the necessary communication between the players.
*  It informs players with `GAME START` command
*  It informs players with `GAME END` command
*  It populates slave player (who is matched as the second player) with the execution of actual game that is played by master player (who is matched as the first player)
*  It informs both players with other player's information such as coordinate, health and score. 

Note: The communication mechanism is built on top of UDP concerning performance related issues.  

### Protocols

Game Server conveys the communication with the following message types/protocols.

*  Entities: Forwards all the screen entities from master player to slave player.
*  PlayersInfo: Reciprocal player information protocol that transmits positions, scores and health information.
*  StartGame: Start game message that initiates both player to understand that the game starts. It contains role information.
*  EndGame: End game message that finishes the game in both players. It contains final scores of both players.

## RESTful API Documentation

### Player Services

##### 1. Get all players

**GET**  ```../players```, returns list of player records

Example Request: ```localhost:8080/players``` 
```json
[
    {
        "id": 20,
        "nickname": "berat"
    },
    {
        "id": 21,
        "nickname": "murat"
    }
]
```

##### 2. Get the player with given player_id

**GET**  ```../players/{id}```, returns record requested player record

Example Request: ```localhost:8080/players/20``` 

```json
{
    "id": 20,
    "nickname": "berat"
}
```

### Authentication Information Services

##### 1. Get all authentication information

**GET**  ```../auth```, returns list of authentication records

Example Request: ```localhost:8080/auth``` 
```json
[
    {
        "id": 20,
        "nickname": "berat",
        "password": "a9ca95d849b26de6cd28a12461cd3a74bf91ba130115c18a10f5bed349352bfdbfdd9e2563198129201bd2a59863e81c4364baf99e2f2d04f22199bfe759b4c8",
        "salt": "java.security.SecureRandom@769efb38"
    },
    {
        "id": 21,
        "nickname": "murat",
        "password": "0756ecd805a1a4a7e3149f6ec30ef24ebcf11e3d16c236b01019a57fc14f28a06d4af2796c3e695f460526ac0b0040d19b57357c5c977bcef445be9120a58fb5",
        "salt": "java.security.SecureRandom@610a82da"
    }
]
```

##### 2. Signing up the user into game, returns id of existing player in case of success

**POST**  ```../auth/signup``` with following payload content, returns id of player
* nickname : String
* password : String

Example Request: ```localhost:8080/auth/signup``` 
* nickname : "Ash"
* password : "Pikacu"

```json
980
```

##### 3. Get authentication information of player with given id

**GET**  ```../auth/{id}```

Example Request: ```localhost:8080/auth/980```

```json
{
    "id": 980,
    "nickname": "Ash",
    "password": "1f0a87c58bf9075a7514149e323ad2ff482a0b1eed6f2a2c0a4b0ac34562d92a9bfb08bdd4826023f8f839570dba1a0cecedd6a0ede6a8c7e4a6f990e3340209",
    "salt": "java.security.SecureRandom@2a02d8f8"
}
```

##### 4. Update authentication information of player with given id, returns updated player id

**PUT**  ```../auth/{id}``` with following payload content, returns updated id of player
* nickname : String
* password : String

Example Request: ```localhost:8080/auth/982```
* nickname : "Yugioh"
* password : "Bad Pegasus"

```json
982
```

**Before**
```json
{
    "id": 982,
    "nickname": "Yugioh",
    "password": "ec975dcd489daaac6c5f555b889cd70f4f62c27cc3f6d0efcbfc0f00e67b5b7b1226844673111db0666b1f93a6dd80ed242e0affe3d30bc670e4c3fef1f4fa50",
    "salt": "java.security.SecureRandom@7bcb5ed8"
}
```
**After**
```json
{
    "id": 982,
    "nickname": "Yugioh009",
    "password": "8f9bed47d577053ec43178a00b4f9e2808655a4eed91a3cb9a0788cabe103875dc33adc9327ca72746d66fd1b5d03080c6ccc3db81e657dc7f210176f5865187",
    "salt": "java.security.SecureRandom@7bcb5ed8"
}
```

##### 5. Deletes player record with the given player id

**DELETE**  ```../auth/{id}```, returns Nothing

Example Request: ```localhost:8080/auth/982```

##### 6. Login the player into game

**POST**  ```../auth/login```with following payload content, returns updated id of player
* nickname : String
* password : String

Example Request: ```localhost:8080/auth/login```
* nickname : "Ash"
* password : "Pikacu"

```json
980
```

### Score Services

##### 1. Get all the time records with given limit

**GET**  ```../scores/all/{limit}```, returns list of all the time score records with given limit

Example Request: ```localhost:8080/scores/all/5``` 
```json
[
    {
        "score_id": 34,
        "player_id": 20,
        "score": 9999,
        "date": "2018-11-14T00:38:34.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    },
    {
        "score_id": 47,
        "player_id": 20,
        "score": 8888,
        "date": "2018-11-14T17:44:21.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    },
    {
        "score_id": 42,
        "player_id": 21,
        "score": 5200,
        "date": "2018-11-14T18:13:13.000+0300",
        "player": {
            "id": 21,
            "nickname": "murat"
        }
    },
    {
        "score_id": 35,
        "player_id": 20,
        "score": 1600,
        "date": "2018-11-14T00:39:44.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    },
    {
        "score_id": 74,
        "player_id": 21,
        "score": 1233,
        "date": "2018-11-16T10:24:23.000+0300",
        "player": {
            "id": 21,
            "nickname": "murat"
        }
    }
]
```

##### 2. Get weekly records with given limit

**GET**  ```../scores/weekly/{limit}```, returns list of weekly score records with given limit

Example Request: ```localhost:8080/scores/weekly/3``` 
```json
[
    {
        "score_id": 34,
        "player_id": 20,
        "score": 9999,
        "date": "2018-11-14T00:38:34.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    },
    {
        "score_id": 47,
        "player_id": 20,
        "score": 8888,
        "date": "2018-11-14T17:44:21.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    },
    {
        "score_id": 42,
        "player_id": 21,
        "score": 5200,
        "date": "2018-11-14T18:13:13.000+0300",
        "player": {
            "id": 21,
            "nickname": "murat"
        }
    }
]
```

##### 3. Get all the time records as stored in database

**GET**  ```../scores/all/raw```, returns list of all the time score records as stored in actual database

Example Request: ```localhost:8080/scores/all/raw``` 
```json
[
    {
        "score_id": 32,
        "player_id": 20,
        "score": 1000,
        "date": "2018-11-14T00:35:22.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    },
    {
        "score_id": 34,
        "player_id": 20,
        "score": 9999,
        "date": "2018-11-14T00:38:34.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    }
]
```
(Above list continues)

##### 4. Get weekly records as stored in database

**GET**  ```../scores/weekly/raw```, returns list of weekly score records as stored in actual database

Example Request: ```localhost:8080/scores/weekly/raw``` 
```json
[
    {
        "score_id": 32,
        "player_id": 20,
        "score": 1000,
        "date": "2018-11-14T00:35:22.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    },
    {
        "score_id": 34,
        "player_id": 20,
        "score": 9999,
        "date": "2018-11-14T00:38:34.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    }
]
```
(Above list continues)

##### 5. Get all the time records as descending order

**GET**  ```../scores/all```, returns list of all the time score records as descending order

Example Request: ```localhost:8080/scores/all``` 
```json
[
    {
        "score_id": 34,
        "player_id": 20,
        "score": 9999,
        "date": "2018-11-14T00:38:34.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    },
    {
        "score_id": 32,
        "player_id": 20,
        "score": 1000,
        "date": "2018-11-14T00:35:22.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    }
]
```
(Above list continues)

##### 6. Get weekly records as descending order

**GET**  ```../scores/weekly```, returns list of weekly score records as descending order

Example Request: ```localhost:8080/scores/weekly``` 
```json
[
    {
        "score_id": 34,
        "player_id": 20,
        "score": 9999,
        "date": "2018-11-14T00:38:34.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    },
    {
        "score_id": 32,
        "player_id": 20,
        "score": 1000,
        "date": "2018-11-14T00:35:22.000+0300",
        "player": {
            "id": 20,
            "nickname": "berat"
        }
    }
]
```
(Above list continues)

##### 7. Add score to both all the time and weekly tables

**POST**  ```../scores``` with following payload content, returns id of player

Example Request: ```localhost:8080/scores/weekly``` 
```json
{
	"player_id" : 980,
	"score": 1090,
	"player" : {
		"id": 980,
		"nickname" : "Ash",
		"password" : "Pikacu"
	}
}
```
An response example,
```json
{
    "score_id": 986,
    "player_id": 980,
    "score": 1090,
    "date": "2018-11-29T18:37:02.524+0300",
    "player": {
        "id": 980,
        "nickname": "Ash"
    }
}
```

##### 8. Add score  all the time table

**POST**  ```../scores/all``` with following payload content, returns id of player

Example Request: ```localhost:8080/scores/weekly``` 
```json
{
	"player_id" : 980,
	"score": 1090,
	"player" : {
		"id": 980,
		"nickname" : "Ash",
		"password" : "Pikacu"
	}
}
```
An response example,
```json
{
    "score_id": 986,
    "player_id": 980,
    "score": 1090,
    "date": "2018-11-29T18:37:02.524+0300",
    "player": {
        "id": 980,
        "nickname": "Ash"
    }
}
```

##### 9. Add score to weekly table

**POST**  ```../scores/weekly```, returns id of player

Example Request: ```localhost:8080/scores/weekly``` 
```json
{
	"player_id" : 980,
	"score": 1090,
	"player" : {
		"id": 980,
		"nickname" : "Ash",
		"password" : "Pikacu"
	}
}
```
An response example,
```json
{
    "score_id": 986,
    "player_id": 980,
    "score": 1090,
    "date": "2018-11-29T18:37:02.524+0300",
    "player": {
        "id": 980,
        "nickname": "Ash"
    }
}
```

##### 10. Update all the time score

**PUT**  ```../scores/all/{id}``` with following example payload content, returns updated id of score

Example Request: ```localhost:8080/scores/all/982```
```json
{
	"player_id" : 980,
	"score": 1110,
	"player" : {
		"id": 980,
		"nickname" : "Ash",
		"password" : "Pikacu"
	}
}
```
An response example,
```json
986
```

##### 11. Update weekly score

**PUT**  ```../scores/weekly/{id}``` with following payload content, returns updated id of score

Example Request: ```localhost:8080/scores/weekly/982```
```json
{
	"player_id" : 980,
	"score": 1110,
	"player" : {
		"id": 980,
		"nickname" : "Ash",
		"password" : "Pikacu"
	}
}
```
An response example,

```json
986
```

##### 12. Delete all the time score

**DELETE**  ```../scores/all/{id}```, returns Nothing

Example Request: ```localhost:8080/scores/all/678```

##### 13. Delete weekly score

**DELETE**  ```../scores/weekly/{id}```, returns Nothing

Example Request: ```localhost:8080/scores/weekly/678```